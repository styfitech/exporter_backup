import os
import requests
import errno
import urllib
import time
import argparse
import logging
import sys
import operator

# Checking if process is already running, if it is not, then starting
pid = str(os.getpid())
pidfile = "/tmp/image_downloader.pid"


def setup_custom_logger(log_name):
    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger_name = logging.getLogger(log_name)
    logger_name.setLevel(logging.INFO)
    logger_name.addHandler(handler)
    return logger_name

logger = setup_custom_logger('root')

if os.path.isfile(pidfile):
    logger.info('%s already exists, exiting' % pidfile)
    sys.exit()

open(pidfile, 'w').write(pid)

APP_HEADER = {'authorization': 'bearer eeey1lR3f48YxYUr9R3m7yQAYb74pDIrJiI'}


def recurse(category_data, parent_name, parent_id, required_cat=None):
    for zxc in category_data["children"]:
        if not zxc["children"]:
            category = {"name": zxc["name"], "parent": parent_name, "parent_id": parent_id}
            category['parent'] += (" > " + zxc["name"])
            category['parent_id'] += (" > " + zxc["category_id"])
            if required_cat == zxc["category_id"]:
                create_folder(required_cat)
                get_product_category(required_cat, required_cat)
                logger.info(category)
                logger.info("category %s: %s" % (category, zxc["category_id"]))
        else:
            zxc_name = parent_name + " > " + zxc["name"]
            zxc_id = parent_id + " > " + zxc["category_id"]
            recurse(zxc, zxc_name, zxc_id, required_cat)


def create_folder(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def get_product_category(path_name, cate_id):
    logger.info('Downloading images for category: %s' % cate_id)
    request_params = {}
    request_params['category_id'] = cate_id
    request_params['limit'] = 100
    request_params['tagged'] = 1
    page = 1
    cat_url = 'http://www.styfi.in/api/product'
    while True:
        request_params['page'] = page
        product_data_json = requests.get(cat_url, headers=APP_HEADER,params=request_params).json()
        product_data = product_data_json['data']
        if not product_data:
            break
        for each_data in product_data:
            images_data = each_data['images']
            index, value = min(enumerate([int(a['sort_order']) for a in images_data]), key=operator.itemgetter(1))
            logger('Image with sort order: %s' % value)
            download_image_folder(images_data[index]['popup'], path_name)
        page += 1


def download_image_folder(image_url, path_name):
    filename = image_url.split('/')[-1]
    logger.info('%s file saved in folder: %s' % (filename, path_name))
    urllib.urlretrieve(image_url, path_name + "/" + filename)


try:
    data_category = requests.get('http://www.styfi.in/api/product/category/tree', headers=APP_HEADER).json()
    category_data_value = data_category['data']
    parser = argparse.ArgumentParser()
    parser.add_argument('-file', nargs='?', help='pass filename')
    args = vars(parser.parse_args())
    if 'file' in args:
        filename = args['file']
    with open(filename) as f:
        content = f.readlines()
    no_category = False
    for x in category_data_value:
        name = x["name"]
        category_id = x['category_id']
        if no_category:
            break
        for category_data in content:
            category_id_ = category_data.strip()
            if category_id_:
                logger.info(category_id_)
                recurse(x, name, category_id, category_id_)
            else:
                no_category = True
                break
finally:
    logger.info('Download complete.')
    logger.info('Un-linking file')
    os.unlink(pidfile)



