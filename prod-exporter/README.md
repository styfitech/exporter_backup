# CODES

## SETTINGS

 `settings.py` contains all the configuration information. Check the values before running the script on a certain
 server.

## STYFI PARTNERS STORE

 + DESCRIPTION
    Script to fetch Partner stores from [API](http://prod.styfi.in/partners/get_partner_stores_minimal) and fetch product
    data for individual stores. Product data is added to queue.

 + PARAMETERS
    1. Store Id (-store): Single store_id can be passed. If passed, only that store products are added to queue.
    If not passed, all store products will be fetched and added to queue.
    2. Skip (-skip): Stores to skip. Should be passed only if running for all stores and one store needs to be skipped.
    3. IDs (-ids): Products IDs to crawl. Store ID field is a must along with this parameter.
    4. Priority (-p): Priority with which they should appear on Feed/Update. Range 1-10.
    5. Spawn (-s): If you are running the process not from cron then use this parameter.

 + EXAMPLE

        nohup python styfi_partners_store.py -s=1 -store=23 -p=2 > /var/log/rabbitmq/store.log &

## DETAILS EXPORTER

 + DESCRIPTION
    Script to pick raw product data from Queue, perform ETL and send data to API.
    RabbitMq management console: [LINK](autocat.styfi.in:15672)
    Queue: details:items
    Vhost: prod_styfi

 + PARAMETERS
    1. Spawn (-s): If you are running the process not from cron then use this parameter. To spawn 1 more consumer.

 + EXAMPLE

        nohup python details_exporter.py -s=1 > /var/log/rabbitmq/details.log &

 + NOTE
    If no consumers are attached to queue, check log file first, if no errors in log run `python soft_kill_process.py`

## DELETE LOGS

 + DESCRIPTION
    Script to delete log files. Last 3 logs files for a script are retained.

## CHECK FOR REVIEW

 + DESCRIPTION
    Script to check if products have inconsistent data. If data is inconsistent, product status is set to -2(Mark for
       review)

 + PARAMETERS
    1. Status (-s): Run for products with a particular status.
    2. Store/Website (-w): Run for a particular partner store.
    3. Brand/manufacturer (-b): Run for a particular Brand.

 + EXAMPLE

        nohup python check_for_review.py -s=1 > /var/log/rabbitmq/review.log &

## STYFI ELASTIC OFFLINE

 + DESCRIPTION
    Script to export data from Mysql (through API) to Elasticsearch.

 + PARAMETERS
    1. All (-a): Run for all indexes.
    2. Brands (-b): Imports only brands to ES.
    3. Categories (-c): Imports only categories to ES.
    4. Products (-p): Imports only products to ES.
    5. Tags (-t): Imports only tags to ES.
    6. User Scores (-u): Imports only user scores to ES. Pass `0` with the -u parameter.

 + ISSUES
    Json Error: Doshi's API is not working. Contact Doshi.

 + NOTES
    `config.py` is the configuration file for this script. Check config before running.

## SERVER SECURITY

 + DESCRIPTION
    Script that notifies about users that logged in the system. Gives the IP of the logged in user. Always check the
    IP in the mail received.

## SERVICE RUNNING

 + DESCRIPTION
    Services to be monitored need to be defined in `settings.py` in `PROCESS_TO_CHECK`. Allowed states of the process
    need to be defined in the Dict. By default, running and sleeping are the states for most of the processes.
    If the process is in some other state, the script will try to start the process and will slack the details.

## SOFT KILL PROCESS

 + DESCRIPTION
    Soft kills detail exporter.

 + EXAMPLE

         python soft_kill_process.py

## STOCK STATUS

 + DESCRIPTION
    Sends an email of the total number of products crawled for the day. Each unique product counted once. Last update
    considered. Always check if the **MARK FOR REVIEW** count is low. If it is greater for any brand, check data in
    mongo - DB: styfi_curated collection: api_live_{date}

## REQUEUE FAILED

 + DESCRIPTION
    Re-Adds products failed while exporting data from queue to API. Products failed due to Queue error or inconsistent
    data are added in queue_error And products failed due to API related errors/Non-success return codes are added to
    api_failed collection. Each product is retried max 4 times.

 + PARAMETERS
    1. Queue (-q): Raw products are picked from queue_failed collection, else if no paramter is passed,
    from api_failed collection.

 + EXAMPLE

        nohup python requeue_failed.py > /var/log/rabbitmq/requeue.log &

 + NOTE:
    Retry count needs to be checked once.



