from datetime import datetime, timedelta
from pymongo import MongoClient
import settings
import subprocess

database = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION]

yesterday = datetime.now() - timedelta(days=1)

subject = "Stock Status for %s" % datetime.now().strftime('%d-%m-%y')

pipeline = [
    {"$match": {"extract_time": {"$gte": yesterday}}},
    {"$sort": {"extract_time": 1}},
    {"$group":
        {
            "_id": {"store": "$product_store.store_name", "styfi_product_id": "$styfi_product_id", "status": "$status"},
            "time": {"$last": "$extract_time"}
        }
     },
    {"$group":
        {
            "_id": {"store": "$_id.store", "status": "$_id.status"},
            "count": {"$sum": 1}
        }
     }
]

body = '''
        <h3>Stock Stats</h3>
        <table style="width:100%;text-align: left;border: 1px solid black;">
          <tr>
	    <th style="border: 1px solid black;">Sl No</th>
            <th style="border: 1px solid black;">Website</th>
            <th style="border: 1px solid black;">In-Stock</th>
            <th style="border: 1px solid black;">Out-of-stock</th>
            <th style="border: 1px solid black;">Discontinued</th>
            <th style="border: 1px solid black;">Mark for Review</th>
          </tr>
          '''


aggregate_response = database[settings.API_COLLECTION_WITHOUT_TIMESTAMP + datetime.now().strftime('%Y-%m-%d')
                              ].aggregate(pipeline=pipeline, allowDiskUse=True)

website_count = {'instock': 0, 'outofstock': 0, 'discontinued': 0, 'review': 0}
status_dict = {}
for stats in aggregate_response:
    product_detail = stats['_id']
    count = stats['count']
    website = product_detail['store']
    if website and isinstance(website, list):
        website = website[0].capitalize()
    if website not in status_dict:
        status_dict[website] = website_count.copy()
    status = product_detail['status']
    if status == -1:
        status_dict[website]['discontinued'] = count
    elif status == 0:
        status_dict[website]['outofstock'] = count
    elif status == 1:
        status_dict[website]['instock'] = count
    elif status == -2:
        status_dict[website]['review'] = count

i=1
for key, value in status_dict.items():
    body += '''<tr><td style="border: 1px solid black;">''' + str(i) + '''</td>
	<td style="border: 1px solid black;">''' + str(key) + '''</td>
        <td style="border: 1px solid black;">''' + str(value['instock']) + '''</td>
        <td style="border: 1px solid black;">''' + str(value['outofstock']) + '''</td>
        <td style="border: 1px solid black;">''' + str(value['discontinued']) + '''</td>
        <td style="border: 1px solid black;">''' + str(value['review']) + '''</td>
        </tr>'''
    i += 1

body += '''
        </table>'''

process = subprocess.Popen(['mutt', '-e', 'set copy=no', '-e', 'set content_type=text/html', '-s', subject,
                            settings.RECIPIENT],
                           stdin=subprocess.PIPE)
process.communicate(body)
