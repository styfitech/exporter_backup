from pymongo import MongoClient
import os
import pika
import json
import settings
import requests
import argparse

# Checking if process is already running, if it is not, then starting
pid = str(os.getpid())
pid_prefix = settings.PID_PREFIX
pidfile = "/tmp/" + pid_prefix + "requeue.pid"

logger = settings.setup_custom_logger('root')

parser = argparse.ArgumentParser()
parser.add_argument('-q', '--queue', nargs='?', help='Pick products from queue failed collection.')
args = vars(parser.parse_args())
if 'queue' in args:
    queue = args['queue']
    if queue:
        api_error = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION][settings.QUEUE_ERR_COLLECTION]
    else:
        api_error = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION][settings.API_ERR_COLLECTION]


def send_slack(msg):
    requests.post(
        settings.SLACK_URL,
        data=json.dumps({
            "channel": "#partner_tracking", "username": "webhookbot",
            "text": msg,
            "icon_emoji": ":styfi:"}))

try:
    routing_key_value = settings.QUEUE.get('product_details', 'details') + ':items'
    virtual_host = settings.RABBITMQ_HOST.get('vhost', '/')
    # Adding urls to RabbitMq for processing by the crawlers
    if settings.credentials:
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port'],
            virtual_host=virtual_host, heartbeat_interval=0, credentials=settings.credentials))
    else:
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port']))

    channel = connection.channel()

    for product in api_error.find({"$or": [{"retry_count": {"$lt": 3}}, {"retry_count": {"$exists": False}}]}):
        product.pop('_id')
        if 'is_in_stock' in product:
            stock_status = int(product['is_in_stock'])
            if 'price' in product:
                # Fetching Domain name from Urls
                channel.basic_publish(exchange='',
                                      routing_key=routing_key_value,
                                      body=json.dumps(product),
                                      properties=pika.BasicProperties(
                                          priority=1
                                      ))
            else:
                messages = "Missing product price info: '{0}'".format(product['url'])
                logger.info(messages)
        else:
            messages = "Stock status missing: : '{0}'.".format(product['url'])
            logger.info(messages)
    connection.close()
finally:
    logger.info(' [x] Process Complete.')
    if os.path.isfile(pidfile):
        os.unlink(pidfile)

