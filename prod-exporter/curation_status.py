from datetime import datetime, timedelta
from pymongo import MongoClient
import settings
import subprocess

yesterday = datetime.now() - timedelta(days=1)
database = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION]

subject = "Curation Status for %s" % datetime.now().strftime('%d-%m-%y')

pipeline = [
    {
        "$match": {
            "time_added": {"$gte": yesterday}
        }
    }
]

aggregate_response = database['stylist_curated_category'].aggregate(pipeline=pipeline)

body = '''
    <h3>Stats</h3>
    <table style="width:100%;text-align: left;border: 1px solid black;">
      <tr>
        <th style="border: 1px solid black;">Username</th>
        <th style="border: 1px solid black;">Category</th>
        <th style="border: 1px solid black;">Brand Name</th>
        <th style="border: 1px solid black;">Website</th>
        <th style="border: 1px solid black;">Crawled Count</th>
      </tr>
      '''

total_count = 0
for curated in aggregate_response:
    username = curated['username']
    keyword = curated['keyword']
    brand = curated['brand']
    try:
        brand_id = curated['brand_id']
        category_id = curated['category_id']
    except KeyError:
        brand_id = None
        category_id = None
    styfi_category = curated['styfi_category']

    pipeline_2 = {
                "extract_time": {"$gte": yesterday},
            }

    if brand_id:
        pipeline_2['brand_id'] = brand_id
    if category_id:
        pipeline_2['category_id'] = category_id
    if keyword:
        pipeline_2['url'] = {"$regex": '.*\Q' + keyword + '\E.*', "$options": 'i'}

    count = database[settings.PRODUCT_COLLECTION].find(pipeline_2).count()
    total_count += count

    body += '''<tr><td style="border: 1px solid black;">''' + str(username) + '''</td>
        <td style="border: 1px solid black;">''' + str(str(styfi_category.replace(u'\xa0', ' '))) + '''</td>
        <td style="border: 1px solid black;">''' + str(brand) + '''</td>
        <td style="border: 1px solid black;">''' + str(keyword) + '''</td>
        <td style="border: 1px solid black;">''' + str(count) + '''</td></tr>'''

body += '''<tr><td colspan="4" style="border: 1px solid black;">Total</td>
        <td style="border: 1px solid black;">''' + str(total_count) + '''</td></tr>
        </table>'''

process = subprocess.Popen(['mutt', '-e', 'set copy=no', '-e', 'set content_type=text/html', '-s', subject,
                            settings.CURATE_RECIPIENT],
                           stdin=subprocess.PIPE)
process.communicate(body)

