#!/bin/bash

MONGO_DATABASE="styfi_curated"
APP_NAME="universal_live_"

MONGO_HOST="127.0.0.1"
MONGO_PORT="27017"
TIMESTAMP=`date +%F -d "-1 month"`
MONGODUMP_PATH="/usr/bin/mongodump/universal_live"
BACKUPS_DIR="/home/ec2-user/mongodump/universal_live/$TIMESTAMP"
BACKUP_NAME="$APP_NAME$TIMESTAMP"
MONGO_PATH="/usr/bin/mongo"

$MONGODUMP_PATH -d $MONGO_DATABASE -c $BACKUP_NAME

mkdir -p $BACKUPS_DIR
mv dump $BACKUP_NAME
tar -zcvf $BACKUPS_DIR/$BACKUP_NAME.tgz $BACKUP_NAME
rm -rf $BACKUP_NAME

$MONGO_PATH --eval "db=db.getSiblingDB('${MONGO_DATABASE}'); db['${BACKUP_NAME}'].drop();"