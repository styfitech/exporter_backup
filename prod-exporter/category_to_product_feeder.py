#!/usr/bin/env python
import os
import sys
import pika
import random
import json
import pickle
from scrapy.http import Request
from scrapy.utils.reqser import request_to_dict
import settings
import signal

# Checking if process is already running, if it is not, then starting
pid = str(os.getpid())
pidfile = "/tmp/category_to_product.pid"

logger = settings.setup_custom_logger('root')

if os.path.isfile(pidfile):
    logger.info('%s already exists, exiting' % pidfile)
    sys.exit()

open(pidfile, 'w').write(pid)


def handler(signum, frame):
    logger.info('Received kill signal. Shutting Down.')
    os.unlink(pidfile)
    sys.exit(1)

signal.signal(signal.SIGTERM, handler)

try:

    if settings.credentials:
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port'], credentials=settings.credentials))
    else:
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port']))
    channel = connection.channel()

    channel.queue_declare(queue='products:items', durable=True, arguments={'x-max-priority': 10})
    logger.info(' [*] Waiting for messages. To exit press CTrL+C')


    def callback(ch, method, properties, body):
        headers = {
            'User-Agent':
                random.choice(settings.USER_AGENTS)
        }
        logger.info(' [x] Received %r' % body)
        scraped_product = json.loads(body)
        if scraped_product['store'] == 'koovs' or scraped_product['store'] == 'yepme':
            url = scraped_product['url']
        else:
            if '?' in scraped_product['url']:
                url = scraped_product['url'].split('?')[0]
            else:
                url = scraped_product['url']
        if 'styfi_category' not in scraped_product:
            scraped_product['styfi_category'] = None
        if 'product_filter' not in scraped_product:
            scraped_product['product_filter'] = None
        if 'breadcrumbs' not in scraped_product:
                scraped_product['breadcrumbs'] = None
        if scraped_product['store'] == 'koovs':
            request_object = Request(url,
                                     callback="parse",
                                     meta={'styfi_category': scraped_product['styfi_category'],
                                           'store': scraped_product['store'],
                                           'original_url': scraped_product['url'],
                                           'breadcrumbs': scraped_product['breadcrumbs'],
                                           'category_id': scraped_product['category_id'],
                                           'brand_id': scraped_product['brand_id'],
                                           'product_filter': scraped_product['product_filter']
                                           },
                                     dont_filter=True,
                                     headers=headers)
        else:
            request_object = Request(url,
                                     callback="parse",
                                     cookies={'webCode': 'IN', 'currency': 'INR'},
                                     meta={'styfi_category': scraped_product['styfi_category'],
                                           'store': scraped_product['store'],
                                           'original_url': scraped_product['url'],
                                           'breadcrumbs': scraped_product['breadcrumbs'],
                                           'category_id': scraped_product['category_id'],
                                           'brand_id': scraped_product['brand_id'],
                                           'product_filter': scraped_product['product_filter']
                                           },
                                     dont_filter=True,
                                     headers=headers)
        msg = pickle.dumps(request_to_dict(request_object), protocol=-1)
        channel.basic_publish(exchange='',
                              routing_key='details:requests',
                              body=msg,
                              properties=pika.BasicProperties(
                                  priority=7
                              )
                              )
        logger.info(' [x] Item feed to details:requests queue')
        ch.basic_ack(delivery_tag=method.delivery_tag)

    channel.basic_consume(callback,
                          queue='products:items')

    channel.start_consuming()
finally:
    connection.close()
    logger.info(' [x] Closing Connection to RabbitMQ.')
    if os.path.isfile(pidfile):
        os.unlink(pidfile)


