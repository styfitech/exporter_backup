import predictionio
import logging
from pymongo import MongoClient
import settings
import pytz
import requests
from datetime import datetime, timedelta
import MySQLdb

results_db_item = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION]['recommendation_train_item']
results_db_item_fetch = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION]['recommendation_train_item_same']
ACCESS_KEY = "GNJjQhKakqusSCE8PaYesgnao8gpExuWC13ByGzoV1eEuoL2-78vRsxmbgEz9CIF"

client = predictionio.EventClient(
    access_key=ACCESS_KEY,
    url="http://127.0.0.1:7070",
    # url="http://192.168.1.200:7070",
    threads=5,
    qsize=500
)

pipeline = [
    {
        "$group": {
            "_id": {
                "brand": "$properties.brand",
                "category": "$properties.categories"
            }
        }
    }
]

db = MySQLdb.connect(host="styfi-rds.cbjdlzx4uat7.ap-south-1.rds.amazonaws.com",  # your host, usually localhost
                     user="styfidbadmin",  # your username
                     passwd="FQy5wlUSiC",  # your password
                     db="styfiv2")  # name of the data base

# you must create a Cursor object. It will let
#  you execute all the queries you need
cur = db.cursor()

aggregate_response = results_db_item.aggregate(pipeline=pipeline, allowDiskUse=True)

# data_category = requests.get(settings.API_URL + '/category', headers=settings.AUTHORIZATION_HEADER).json()
# category_system = data_category['data']
# gender_mapping = {'Men': [], 'Women': []}
# for category in category_system:
#     if 'Women' in category['name']:
#         gender_mapping['Women'].append(category['category_id'])
#     else:
#         gender_mapping['Men'].append(category['category_id'])
# print all the first cell of all the rows
for row in aggregate_response:
    product_data = row['_id']
    category = product_data['category'][0]
    brand = product_data['brand'][0]
    request_params = {'manufacturer_id': brand, 'category_id':  category, 'limit': 3}
    data_category = requests.get(settings.API_URL + '/product/live_products', headers=settings.AUTHORIZATION_HEADER,
                                 params=request_params).json()
    product_info = data_category['data']
    properties = {
            'categories': [str(category)],
            'brand': [str(brand)],
            #'updateDate': str(date_start.replace(tzinfo=pytz.timezone('Asia/Calcutta')))
        }
    for product in product_info:
        product_id = product['product_id']
        cur.execute('''SELECT t4.product_id, t4.manufacturer_id, t2.filter_id, t4.tagged, t4.date_added
        FROM styfiv2.oc_product as t4
        INNER JOIN styfiv2.oc_product_filter as t2
        ON t2.product_id=t4.product_id
        WHERE t2.product_id=''' + str(product_id) + ';')
        try:
            try:
                cursor_data = list(cur.fetchall())
                tagged_value = cursor_data[0][3]
                if tagged_value == 1:
                    tagged = ["True"]
                else:
                    tagged = ["False"]
                # if category in gender_mapping['Women']:
                #     gender = ['Female']
                # else:
                #     gender = ['Male']
                date_start = cursor_data[0][4]
                filter_ids = [str(a[2]) for a in cursor_data]
                #properties['gender'] = gender
                properties['tagged'] = tagged
                properties['filters'] = filter_ids
            except IndexError:
                cur.execute('SELECT tagged, date_added FROM styfiv2.oc_product where product_id=' + str(product_id) + ';')
                cursor_data = list(cur.fetchall())
                date_start = cursor_data[0][1]
                tagged_value = cursor_data[0][0]
                if tagged_value == 1:
                    tagged = ["True"]
                else:
                    tagged = ["False"]
                # if category in gender_mapping['Women']:
                #     gender = ['Female']
                # else:
                #     gender = ['Male']
                #properties['gender'] = gender
                properties['tagged'] = tagged
            event_dict = {
                'event': "$set",
                'entity_id': product_id,
                'entity_type': "item",
                'event_time': date_start.replace(tzinfo=pytz.utc),
                'properties': properties
            }
            print event_dict
            mongo_insert = results_db_item_fetch.insert_one(event_dict)
            if mongo_insert.inserted_id:
                logging.info('Product added into DB.')
            #logging.info('Creating an event for track id: %d' % row[0])
            # A user rates an item

            client.create_event(
                event="$set",
                entity_id=product_id,
                entity_type="item",
                properties=properties,
                event_time=date_start.replace(tzinfo=pytz.utc)
            )
        except Exception as e:
            pass


logging.info('terminating')

