#!/usr/bin/env python
import os
import sys
import pika
import json
from pymongo import MongoClient
import pickle
from scrapy.http import Request
from scrapy.utils.reqser import request_to_dict
import settings
import requests
import tldextract
import argparse
from urllib import unquote
import math
import string
import random
from datetime import datetime
import getpass
import time
import validators

logger = settings.setup_custom_logger('root')

# Checing which user is running
logger.info('user account %s' % getpass.getuser())
logger.info('Time re-crawl started %s' % str(datetime.now()))

# Checking if process is already running, if it is not, then starting
pid = str(os.getpid())
pidfile = "/tmp/live_product_recrawl.pid"


if os.path.isfile(pidfile):
    logger.info('%s already exists, exiting' % pidfile)
    sys.exit()

open(pidfile, 'w').write(pid)


def separate_urls(list_to_be_sorted, stopword=None):
    if stopword:
        products_with_urls = [li for li in list_to_be_sorted if li['store_url'] and stopword not in li['store_url']]
    else:
        products_with_urls = [li for li in list_to_be_sorted if li['store_url']]
    products_without_ids = [li['product_id'] for li in list_to_be_sorted if not li['store_url']]
    if products_without_ids:
        logger.warn('Products with missing IDs')
        logger.warn(products_without_ids)
    sorted_list = sorted(products_with_urls, key=lambda k: k['store_url'])
    serparated_urls = []

    for m, n in zip(sorted_list, reversed(sorted_list)):
        domain_start = tldextract.extract(m['store_url'])
        store_start = domain_start.domain
        domain_end = tldextract.extract(n['store_url'])
        store_end = domain_end.domain
        if store_start != store_end:
            serparated_urls.append(m)
            serparated_urls.append(n)
        else:
            serparated_urls.append(m)
        if len(serparated_urls) == len(sorted_list):
            break
    return serparated_urls


def unique_id(prefix='', more_entropy=False):
    m = time.time()
    unique_id = '%8x%05x' % (math.floor(m), (m - math.floor(m)) * 1000000)
    if more_entropy:
        valid_chars = list(set(string.hexdigits.lower()))
        entropy_string = ''
        for i in range(0, 10, 1):
            entropy_string += random.choice(valid_chars)
        unique_id = unique_id + entropy_string
    unique_id = prefix + unique_id
    return unique_id


try:

    routing_key_value = 'recrawl:requests'
    store_data = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION]['universal_live_recrawl']
    # Adding urls to RabbitMq for processing by the crawlers
    if settings.credentials:
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port'], credentials=settings.credentials))
    else:
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port']))

    channel = connection.channel()

    category_id = None
    brand_id = None

    page = 1
    more_products = True

    parser = argparse.ArgumentParser()
    parser.add_argument('-priority', nargs='?', help='pass priority for crawl')
    parser.add_argument('-keyword', nargs='?', help='pass keywords present in urls to search')
    parser.add_argument('-limit', nargs='?', help='no. of live product urls to fetch from API')
    parser.add_argument('-store', nargs='?', help='Store id to re-crawl from')
    parser.add_argument('-status', nargs='?', help='current status of the products')
    parser.add_argument('-collection', nargs='?', help='collection id')
    parser.add_argument('-product', nargs='?', help='product id to pass')
    parser.add_argument('-brand', nargs='?', help='brand id to pass')
    parser.add_argument('-stopword', nargs='?', help='stores not to crawl')
    args = vars(parser.parse_args())
    if 'priority' in args:
        priority = args['priority']
        if priority:
            priority = int(priority)
            logger.info('Priority: %s' % priority)
        else:
            priority = 1
            logger.info('Setting normal priority: %s.' % priority)
    if 'brand' in args:
        brand_id = args['brand']
        logger.info('brand to be used to filter urls: %s' % brand_id)
    if 'keyword' in args:
        keyword = args['keyword']
        logger.info('Keyword to be used to filter urls: %s' % keyword)
    if 'product' in args:
        product_id = args['product']
        logger.info('Product id to crawl: %s' % product_id)
    if 'limit' in args:
        limit = args['limit']
    if 'store' in args:
        store_id = args['store']
        logger.info('Store ID to be used to filter urls: %s' % store_id)
    if 'status' in args:
        status = args['status']
        logger.info('Status to be used to filter urls: %s' % status)
    if not limit:
        limit = 500
    if 'collection' in args:
        collection = args['collection']
        logger.info('Collection Id: %s' % collection)
    if 'stopword' in args:
        stopword = args['stopword']
        logger.info('Store not to crawl: %s' % stopword)
    logger.info('no. of live product urls to fetch from API: %s' % limit)

    while more_products:
        request_params = {'page': page, 'limit': limit}
        if store_id:
            request_params['store_id'] = store_id
        if status:
            request_params['status'] = status
        if collection:
            request_params['collection_id'] = collection
        if product_id:
            request_params['product_id'] = product_id
        if brand_id:
            request_params['manufacturer_id'] = brand_id
        data_category = requests.get(settings.API_URL + '/product/live_products', headers=settings.AUTHORIZATION_HEADER,
                                     params=request_params)

        data_dict = json.loads(data_category.text)
        crawling_data_urls = data_dict['data']
        if not crawling_data_urls:
            more_products = False
            break
        separate_urls_data = separate_urls(crawling_data_urls, stopword)

        for product in separate_urls_data:
            # Fetching Domain name from Urls
            try:
                valid_url = validators.url(product['store_url'])
                if not valid_url:
                    logger.error('Malformed url: %s' % product['store_url'])
                    break
                domain_data = tldextract.extract(product['store_url'])
                store = domain_data.domain
                headers = {
                    'User-Agent':
                        random.choice(settings.USER_AGENTS)
                }
                if 'styfi_category' not in product:
                    product['styfi_category'] = None

                if store and (not keyword or keyword in product['store_url']):
                    product['store_url'] = unquote(product['store_url'].replace('&amp;', '&'))
                    # Handling Redirects for Koovs
                    if store == 'koovs':
                        req = Request(product['store_url'],
                                      callback="parse",
                                      meta={'styfi_category': product['styfi_category'],
                                            'store': store,
                                            'category_id': product['category_id'],
                                            'brand_id': product['manufacturer_id'],
                                            'sku': product['sku'],
                                            'recrawl': True,
                                            'product_id': product['product_id']
                                            #'assign_to': product['assigned_user_id']
                                            },
                                      dont_filter=True,
                                      headers=headers
                                      )
                    else:
                        if store == 'amazon':
                            product['store_url'] = product['store_url'].replace('/ref=as_li_tl', '')
                            product['store_url'] = (product['store_url'].replace('&tag=wwwstyfiin-21', '')
                                                    .replace('&SubscriptionId=AKIAICEFJ77BQDQZBS6A', ''))
                        req = Request(product['store_url'],
                                      callback="parse",
                                      cookies={'webCode': 'IN', 'currency': 'INR'},
                                      meta={'styfi_category': product['styfi_category'],
                                            'store': store,
                                            'category_id': product['category_id'],
                                            'brand_id': product['manufacturer_id'],
                                            'sku': product['sku'],
                                            'recrawl': True,
                                            'product_id': product['product_id']
                                            #'assign_to': product['assigned_user_id']
                                            },
                                      dont_filter=True,
                                      headers=headers
                                      )
                    msg = pickle.dumps(request_to_dict(req), protocol=-1)
                    channel.basic_publish(exchange='',
                                          routing_key=routing_key_value,
                                          body=msg,
                                          properties=pika.BasicProperties(
                                              priority=priority
                                          ))
                    try:
                        inserted_data = store_data.insert_one(product)
                    except Exception as e:
                        product['_id'] = unique_id()
                        inserted_data = store_data.insert_one(product)
                    if inserted_data.inserted_id:
                        logger.info('Product with url %s added into DB.' % product['store_url'])
            except Exception as e:
                logger.error('Product with url %s could not be crawled.' % product['store_url'])
        logger.info('page no: %s' % page)
        page += 1
        time.sleep(90)
finally:
    connection.close()
    logger.info('unlinking file')
    os.unlink(pidfile)
