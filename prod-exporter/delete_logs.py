#!/usr/bin/env python
import os
# import shutil
from collections import defaultdict

directory = '/var/log/rabbitmq/'

groups = defaultdict(list)

for filename in os.listdir(directory):
    basename, extension = os.path.splitext(filename)
    try:
        filem, _ = basename.split('.')
        groups[filem].append(filename)
    except ValueError:
        groups[basename].append(filename)

for name, file_names in groups.items():
    file_names.sort()
    file_names = file_names[3:]
    for each_file in file_names:
        file_path = os.path.join(directory, each_file)
        try:
            if os.path.isfile(file_path):
                print('Deleting file {0}'.format(each_file))
                os.unlink(file_path)
                # elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)
