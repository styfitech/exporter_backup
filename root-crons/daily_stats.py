author__ = 'shadow-walker'

mongo_host = '192.168.1.200'
mongo_port = '27017'
mongo_database = 'opencart_tutorial'

from pymongo import MongoClient
from datetime import timedelta, datetime
from tabulate import tabulate

mongoConnection = MongoClient('mongodb://' + mongo_host + ':' + mongo_port + '/')
mongoDBConn = mongoConnection[mongo_database]

mongo_finished = mongoDBConn['oc_finished']
mongo_category = mongoDBConn['oc_category']

category_map = dict()
for result in mongo_category.find():
    category_map[result['category_id']] = result['path'] + ' &gt; ' + result['category_description']['en']['name']

moderated_count = 0
tagged_count = 0
tagged_category_count = dict()
moderated_category_count = dict()
today_where = datetime.now() - timedelta(hours=datetime.now().hour, minutes=datetime.now().minute,
                                         seconds=datetime.now().second)
for result in mongo_finished.find(
        {'$or': [{'tagged_date': {'$gte': today_where}}, {'modify_tagged_date': {'$gte': today_where}}]}):
    tagged_count += 1
    if 'moderated_by' in result:
        moderated_count += 1
        if result['product_category'][0] in tagged_category_count:
            moderated_category_count[result['product_category'][0]] += 1
        else:
            moderated_category_count[result['product_category'][0]] = 1

    if result['product_category'][0] in tagged_category_count:
        tagged_category_count[result['product_category'][0]] += 1
    else:
        tagged_category_count[result['product_category'][0]] = 1


result = list()
for category in category_map:
    temp = dict()
    temp['category'] = category_map[category]
    temp['tagged'] = 0
    temp['moderated'] = 0
    if category in tagged_category_count:
        temp['tagged'] = tagged_category_count[category]

    if category in moderated_category_count:
        temp['moderated'] = moderated_category_count[category]

    if temp['tagged'] or temp['moderated']:
        result.append(temp)

print(tabulate(result, headers="keys", tablefmt="html"))
