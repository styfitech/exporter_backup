#! /bin/sh

# Run this after installation of PM2
sudo echo "alias pm2='env HOME=/var/www pm2'" > /etc/profile.d/00-aliases.sh
pm2 set pm2-logrotate:max_size 100K
pm2 set pm2-logrotate:compress true
pm2 set pm2-logrotate:rotateInterval '*/20 * * * *'
pm2 set pm2-logrotate:retain 3