import os
import signal

path = '/tmp/'


def kill_process(files):
    for file_ in files:
        f = open("/tmp/" + file_, "r")
        pid = f.read()

        print('Soft killing process so that process can cleanup resources')
        os.kill(int(pid), signal.SIGTERM)

files_detail = [i for i in os.listdir(path) if os.path.isfile(os.path.join(path, i)) and
                'detail_extract' in i]

kill_process(files_detail)

files_recrawl = [i for i in os.listdir(path) if os.path.isfile(os.path.join(path, i)) and
                 'recrawl_extract' in i]

kill_process(files_recrawl)



