from pymongo import MongoClient

from backup import settings

database = MongoClient(settings.STYFI_MONGODB)['styfi']

pipeline = [
    { "$match": {
    "user": {"$exists": True},
    "manufacturer_details.manufacturer_id": {"$ne": 755}
}},
{ "$group": {
          "_id" : { "product_id": "$user.product_id"},
          "dups" : {"$addToSet": "$_id"},
          "count": {"$sum": 1}
}},
{ "$match": {
    "count": {"$gt": 1}
}}
]

aggregate_response = database['product_feed_data'].aggregate(pipeline=pipeline, allowDiskUse=True)

for stats in aggregate_response:
    print('deleting id')
    print(stats['dups'][1])
    #database['product_feed_data'].delete_one({'_id': stats['dups'][1]})
