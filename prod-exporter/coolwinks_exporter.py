import requests
from xml.etree import ElementTree
import settings
import pika
import logging
from pymongo import MongoClient
import pickle
from scrapy.http import Request
from scrapy.utils.reqser import request_to_dict
import time
import random
import math
import string

response = requests.get('https://www.coolwinks.com/sitemap/category-product.xml')

tree = ElementTree.fromstring(response.content)

ns = {'url_set': 'http://www.sitemaps.org/schemas/sitemap/0.9'}

store_data = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION][settings.CATEGORY_COLLECTION + '_test']

# Brands for Coolwinks
brands = {
    'Xstylfirst': 647,
    'Graviate': 648,
    'Xstyl': 649,
    "Jrs": 645,
    'Laexpress': 646
}

category_mapping = {
    'women': {
        'sunglasses': ['Women > Accessories > Sunglasses', 79],
        'eyeglasses': ['Women > Accessories > Eyeglasses', 80]
    },
    'men': {
        'sunglasses': ['Men > Accessories > Sunglasses', 162],
        'eyeglasses': ['Men > Accessories > Eyeglasses', 80]
    }
}


def unique_value(prefix='', more_entropy=False):
    m = time.time()
    unique_id = '%8x%05x' % (math.floor(m), (m - math.floor(m)) * 1000000)
    if more_entropy:
        valid_chars = list(set(string.hexdigits.lower()))
        entropy_string = ''
        for i in range(0, 10, 1):
            entropy_string += random.choice(valid_chars)
        unique_id = unique_id + entropy_string
    unique_id = prefix + unique_id
    return unique_id

routing_key_value = 'details:requests'
# Adding urls to RabbitMq for processing by the crawlers
if settings.credentials:
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port'], credentials=settings.credentials))
else:
    connection = pika.BlockingConnection(pika.ConnectionParameters(
                    host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port']))

channel = connection.channel()
priority_crawl = 8
# for child in tree:
#     for child_of_child in child:
#         print child_of_child.tag, child_of_child.text


for url_field in tree.findall('url_set:url', ns):
    headers = {
        'User-Agent':
            random.choice(settings.USER_AGENTS)
    }
    url = url_field.find('url_set:loc', ns).text
    priority = url_field.find('url_set:priority', ns).text
    if float(priority) < 0.85 and len(url.split('/')) == 6:
        product_data = {}
        protocol, _, domain, category, product, __ = url.split('/')
        try:
            if 'women' in product:
                product_data['category_id'] = category_mapping['women'][category][1]
                product_data['styfi_category'] = category_mapping['women'][category][0].split(' > ')
            else:
                product_data['category_id'] = category_mapping['men'][category][1]
                product_data['styfi_category'] = category_mapping['men'][category][0].split(' > ')
        except KeyError:
            product_data['category_id'] = None
        product_data['store'] = 'coolwinks'
        product_data['brand_id'] = None
        product_data['url'] = url

        for key in brands.keys():
            if key in product:
                product_data['brand_id'] = brands[key]
                break
        if product_data['brand_id'] and product_data['category_id']:
            req = Request(product_data['url'],
                          callback="parse",
                          meta={'styfi_category': product_data['styfi_category'],
                                'store': product_data['store'],
                                'category_id': product_data['category_id'],
                                'brand_id': product_data['brand_id']},
                          dont_filter=True,
                          headers=headers)

            msg = pickle.dumps(request_to_dict(req), protocol=-1)
            channel.basic_publish(exchange='',
                                  routing_key=routing_key_value,
                                  body=msg,
                                  properties=pika.BasicProperties(
                                      priority=priority_crawl
                                  ))
            try:
                inserted_data = store_data.insert_one(product_data)
            except Exception as e:
                product_data['_id'] = unique_value()
                inserted_data = store_data.insert_one(product_data)
            if inserted_data.inserted_id:
                logging.info('Product with url %s added into DB.' % product_data['url'])


