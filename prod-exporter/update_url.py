import MySQLdb
from pymongo import MongoClient

from backup import settings

database = MongoClient(settings.STYFI_MONGODB)['styfi_curated']

db = MySQLdb.connect(host="styfi-rds.cbjdlzx4uat7.ap-south-1.rds.amazonaws.com",  # your host, usually localhost
                     user="styfidbadmin",  # your username
                     passwd="FQy5wlUSiC",  # your password
                     db="styfiv2")  # name of the data base

# you must create a Cursor object. It will let
#  you execute all the queries you need
cur = db.cursor()

# Use all the SQL you like
cur.execute(
    'SELECT product_id, store_id, sku FROM styfi_stores.oc_product_to_store where url like "%http://dapper-homme.myshopify.com/products/baby-pink%";')

for row in cur.fetchall():
    product_id = row[0]
    store_id = row[1]
    sku = row[2]
    try:
        query = {'product_store.store_id': store_id, 'product_store.sku': int(sku)}
    except ValueError:
        query = {'product_store.store_id': store_id, 'product_store.sku': sku}
    feed_data = database['api_live_2017-06-01'].find_one(query)
    try:
        url = feed_data['product_store'][0]['url']
        print('Updating product url')
        cur.execute("UPDATE styfi_stores.oc_product_to_store SET url='%s' WHERE product_id=%d" % (url, product_id))
        db.commit()
    except Exception as e:
        print(store_id)
        print(product_id)
        db.rollback()
        print('Product Not found')

db.close()



