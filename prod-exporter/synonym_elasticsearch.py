import requests
import json
from elasticsearch import Elasticsearch

es = Elasticsearch([{'host': '127.0.0.1', 'port': 9200}])
# es = Elasticsearch([{'host': '192.168.1.200', 'port': 9200}])

settings = {
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0
    },
    "mappings": {
        "urls": {
            "properties": {
                "url": {
                    "type": "string"
                }
            }
        }
     }
}
# create index
es.indices.create(index="", ignore=400, body=settings)

es.update(index='sl_tags', doc_type='news', id=hit.meta.id,
          body={"doc": {"synonym": ["interlaced", "netted", "interlinked"]}})

res = es.search(index="sl_tags", body={"query": {"match": {"filter_id": 21}}})
# logging.info("Got %d Hits:" % res['hits']['total'])
# for hit in res['hits']['hits']:
#     logging.info("%(timestamp)s %(author)s: %(text)s" % hit["_source"])