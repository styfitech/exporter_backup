import MySQLdb
import predictionio
import logging
from pymongo import MongoClient
import settings
import pytz

results_db = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION]['recommendation_train']

ACCESS_KEY = "GNJjQhKakqusSCE8PaYesgnao8gpExuWC13ByGzoV1eEuoL2-78vRsxmbgEz9CIF"

client = predictionio.EventClient(
    access_key=ACCESS_KEY,
    # url="http://192.168.1.200:7070",
    url="http://127.0.0.1:7070",
    threads=5,
    qsize=500
)

event_weightage = {
    'view': 1,
    'notify': 2,
    'wishlist': 3,
    'share': 1,
    'buy': 5
}

action_to_event_mapping = {
    'product_details': 'view',
    'buy_button': 'buy'
}

db = MySQLdb.connect(host="styfi-rds.cbjdlzx4uat7.ap-south-1.rds.amazonaws.com",    # your host, usually localhost
                     user="styfidbadmin",         # your username
                     passwd="FQy5wlUSiC",  # your password
                     db="styfiv2")        # name of the data base

# you must create a Cursor object. It will let
#  you execute all the queries you need
cur = db.cursor()

# Use all the SQL you like
cur.execute('SELECT * FROM styfiv2.sl_customer_tracking_detail where view_type="buy_button" and customer_id <> 0 ORDER BY customer_id;')

# print all the first cell of all the rows
for row in cur.fetchall():
    customer_id = row[1]
    product_id = row[2]
    event_type = action_to_event_mapping[row[3]]
    view_count = row[4]
    date_added = row[5]
    date_update = row[6]
    event_dict = {
        'event': event_type,
        'entity_type': "user",
        'entity_id': customer_id,
        'target_entity_type': "item",
        'target_entity_id': product_id,
        'event_time': date_update.replace(tzinfo=pytz.utc),
        'properties': {}
    }

    #if event_type == 'view':
    #    event_dict['properties']['view_count'] = [str(view_count)]
    properties = event_dict['properties']
    mongo_insert = results_db.insert_one(event_dict)
    if mongo_insert.inserted_id:
        logging.info('Product added into DB.')
    logging.info('Creating an event for track id: %d' % row[0])
    # A user rates an item
    if event_type == 'view':
        client.create_event(
            event=event_type,
            entity_type="user",
            entity_id=customer_id,
            target_entity_type="item",
            target_entity_id=product_id,
            properties=properties,
            #properties={"view_count": [str(view_count)]},
            event_time=date_update.replace(tzinfo=pytz.utc)
        )
    else:
        client.create_event(
            event=event_type,
            entity_type="user",
            entity_id=customer_id,
            target_entity_type="item",
            target_entity_id=product_id,
            properties=properties,
            event_time=date_update.replace(tzinfo=pytz.utc)
        )

logging.info('terminating')
db.close()
