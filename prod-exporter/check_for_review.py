#!/usr/bin/env python
import os
import sys
import signal
import requests
import subprocess
import argparse

import settings
from pymongo import MongoClient
from datetime import datetime
import traceback

import eventlet
#eventlet.monkey_patch()

'''

    This script fetches products from get_elastic_search_products admin api,
     Checks for certain criteria's and compares for duplicates, if any of the
     checks fail or duplicates exists, sets the status as -2 and updates the products
     through the API

'''

review_reasons = {
    1: 'Images Missing.',
    2: 'Original Price less than Price.',
    3: 'Price less than 50.',
    4: 'Variant Price less than 50.',
    5: 'Size not present in product.',
    6: 'Duplicate Sizes.',
    7: 'Inventory quantity for variants 0.',
    8: 'Inventory quantity 0.',
    9: 'Product missing on website.',
    10: 'Product not updated in last crawl.',
    11: 'Duplicate Product.',
    12: 'Product Missing in Mongodb.'
}

brand_counter = {}

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--status', nargs='?', help='run for a particular status')
parser.add_argument('-w', '--store', nargs='?', help='run for a store id')
parser.add_argument('-b', '--brand', nargs='?', help='run for a brand')
parser.add_argument('-a', '--arguments', nargs='?', help='check for individual conditions. Comma separated values.')
args = vars(parser.parse_args())

# Checking if process is already running, if it is not, then starting
pid = str(os.getpid())
pid_prefix = settings.PID_PREFIX
pidfile = "/tmp/" + pid_prefix + "review.pid"

api_data = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION][settings.API_COLLECTION]
product_collection = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_PRODUCT_DB][settings.STYFI_PRODUCT_COLLECTION]

logger = settings.setup_custom_logger('root')

if os.path.isfile(pidfile):
    logger.warn('%s already exists, exiting' % pidfile)
    sys.exit()

open(pidfile, 'w').write(pid)

# Signal handler to clean resources on shutdown


def handler(signum, frame):
    logger.info('Received kill signal. Shutting Down.')
    os.unlink(pidfile)
    sys.exit(1)

signal.signal(signal.SIGTERM, handler)


def get_stores():
    store_data_ids = None
    store_url = settings.API_URL + "/store"
    store_response = requests.get(store_url, headers=headers, timeout=200).json()
    store_data = store_response.get('data', None)
    if store_data:
        store_data_ids = [int(store['store_id']) for store in store_data if store['has_sheet'] == '1']
    return store_data_ids


def check_product(product_data):
    # For email
    review_status = {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
        7: 0,
        8: 0,
        9: 0,
        10: 0,
        11: 0,
        12: 0
    }
    store_data = product_data.get('stores', None)[0]
    manufacturer_name = product_data.get('manufacturer_name', None)
    if manufacturer_name not in brand_counter:
        brand_counter[manufacturer_name] = review_status
    # store_id = store_data['store_id']
    stock_status = product_data.get('status', None)
    price = float(store_data.get('price', 0))
    original_price = float(store_data.get('original_price', 0))
    if 1 in arguments:
        if not product_data.get('images', None):
            reason = 1
            brand_counter[manufacturer_name][reason] += 1
            # review_status[reason] += 1
            return {'reason': review_reasons[reason], 'reason_code': reason, 'Passed': False}

    if 2 in arguments:
        # Original Price less than Price check
        if original_price < price:
            reason = 2
            brand_counter[manufacturer_name][reason] += 1
            # review_status[reason] += 1
            return {'reason': review_reasons[reason], 'reason_code': reason, 'Passed': False}
    if 3 in arguments:
        # Price less than 50 check
        if price < 50:
            reason = 3
            brand_counter[manufacturer_name][reason] += 1
            # review_status[reason] += 1
            return {'reason': review_reasons[reason], 'reason_code': reason, 'Passed': False}

    # Variant Price less than 50
    try:
        product_options = product_data['product_options'][0]['options']
        variant_price_list = [float(option['price']) for option in product_options]
        variant_quantity_list = [int(option['quantity']) for option in product_options]
        min_variant_price = min(variant_price_list)
        if 4 in arguments:
            if min_variant_price < 50:
                reason = 4
                brand_counter[manufacturer_name][reason] += 1
                # review_status[reason] += 1
                return {'reason': review_reasons[reason], 'reason_code': reason, 'Passed': False}
        category_list = [category['category_id'] for category in product_data['categories']]
        category_intersect = list(set(category_list).intersection(settings.CATEGORIES_WITHOUT_SIZES))
        variant_size_list = [option['option_name'] for option in product_options]
        if 5 in arguments:
            if not category_intersect and not variant_size_list:
                reason = 5
                brand_counter[manufacturer_name][reason] += 1
                # review_status[reason] += 1
                return {'reason': review_reasons[reason], 'reason_code': reason, 'Passed': False}
        if 6 in arguments:
            # Size duplicates
            if len(variant_size_list) != len(list(set(variant_size_list))):
                reason = 6
                brand_counter[manufacturer_name][reason] += 1
                # review_status[reason] += 1
                return {'reason': review_reasons[reason], 'reason_code': reason, 'Passed': False}
        # Inventory quantity
        inventory_quantity = sum(variant_quantity_list)
        if 7 in arguments:
            if stock_status != 0 and not inventory_quantity:
                reason = 7
                brand_counter[manufacturer_name][reason] += 1
                # review_status[reason] += 1
                return {'reason': review_reasons[reason], 'reason_code': reason, 'Passed': False}
    except Exception as err:
        logger.warn('Product: {0} has invalid Variant data.\n{1}'.format(product_data.get('styfi_product_id', None),
                                                                         err))
    quantity = product_data.get('quantity', 0)
    if 8 in arguments:
        if stock_status != 0 and not quantity:
            reason = 8
            brand_counter[manufacturer_name][reason] += 1
            # review_status[reason] += 1
            return {'reason': review_reasons[reason], 'reason_code': reason, 'Passed': False}
    product_url = store_data.get('product_url', None)
    if 9 in arguments:
        if product_url:
            logger.warn('Sending request to website {0}.'.format(product_url))
            try:
                with eventlet.Timeout(30, False):
                    website_response = requests.get(product_url, verify=False)
                    if website_response.status_code > 400:
                        reason = 9
                        brand_counter[manufacturer_name][reason] += 1
                        # review_status[reason] += 1
                        return {'reason': review_reasons[reason], 'reason_code': reason, 'Passed': False}
            # except (requests.exceptions.ConnectionError, requests.exceptions.Timeout):
            except Exception as e:
                logger.warn('Error in getting response from store url. {0}'.format(product_data.get('styfi_product_id', None
                                                                                                    )))
    return {'Passed': True, 'reason': None, 'reason_code': None}


def api_response_generator(received_product):
    dummy_product_data = {"product_description":
        {"1":
            {
                "name": "",
                "description": "",
                "meta_title": "Product Meta",
                "meta_description": "",
                "meta_keyword": "",
                "tag": ""
            }
        },
        "model": "",
        "sku": "",
        "upc": "",
        "ean": "",
        "jan": "",
        "isbn": "",
        "mpn": "",
        "location": "",
        "price": "",
        "tax_class_id": "",
        "quantity": "",
        "minimum": "",
        "subtract": "",
        "stock_status_id": "",
        "shipping": "",
        "keyword": "",
        "date_available": "",
        "length": "",
        "width": "",
        "height": "",
        "length_class_id": "",
        "weight": "",
        "weight_class_id": "",
        "status": "",
        "sort_order": "",
        "manufacturer": "",
        "manufacturer_id": "",
        "category": "",
        "product_category": [],
        "filter": "",
        "product_filter": [],
        "product_store": [
            {"store_id": "",
             "price": "",
             "original_price": "",
             "url": "",
             "discount": ""
             }
        ],
        "download": "",
        "related": "",
        "option": "",
        "image": [],
        "points": "",
        "product_reward": {"1": {"points": ""}},
        "product_layout": ["", "", ""]
    }
    final_product_data = dummy_product_data.copy()
    final_product_data['styfi_product_id'] = received_product.get('styfi_product_id', None)
    store_data = received_product.get('stores', None)
    final_product_data['product_store'] = store_data
    final_product_data['product_id'] = received_product.get('product_id')
    return final_product_data


def update_status(product, status):
    store_data = product['stores'][0]
    store_id_ = store_data['store_id']
    styfi_product_id = product.get('styfi_product_id', None)
    date_modified = product.get('date_modified', None)
    updated_today = True
    if date_modified:
        date_modified = datetime.strptime(date_modified, '%Y-%m-%d %H:%M:%S')
        date_diff = datetime.now() - date_modified
        if date_diff.days > 1:
            updated_today = False
    if updated_today and not status['reason']:
        return None

    store_url = store_data.get('product_url', None)
    logger.info(' [X] MONGO DB query.')
    start_mongo_time = datetime.now()
    if store_url:
        product_count = product_collection.find({"store_data.url": store_url})
    else:
        product_count = product_collection.find({"styfi_product_id": styfi_product_id})
    end_mongo_time = datetime.now()
    mongo_diff = end_mongo_time - start_mongo_time
    logger.info(' [X] Time taken by MONGO: {0} secs'.format(mongo_diff.seconds))
    if len(list(product_count)) > 1:
        status['reason_code'] = 11
        status['reason_code'] = review_reasons[11]

    if product_count:
        if status.get('reason', None):
            product_found = api_response_generator(product)
        elif not updated_today:
            status['reason_code'] = 10
            status['reason_code'] = review_reasons[10]
            product_found = api_response_generator(product)
        else:
            return None
    elif not product_count:
        if store_id_ in store_ids:  # Excel Products
            if status.get('reason', None):
                product_found = api_response_generator(product)
            else:
                return None
        else:
            if not status.get('reason', None):
                status['reason_code'] = 12
                status['reason_code'] = review_reasons[12]
            product_found = api_response_generator(product)
    else:
        return None

    product_found['status'] = -2
    product_found['reason'] = status['reason']
    product_found['reason_code'] = status['reason_code']
    # try:
    #     data = requests.post(settings.API_URL + '/product/update_product_status',
    #                          data={
    #                              'styfi_product_id': styfi_product_id,
    #                              'product_id': product_found['product_id'],
    #                              'status': product_found['status'],
    #                              'reason': product_found['reason'],
    #                              'reason_code': product_found['reason_code']
    #                                },
    #                          headers=settings.AUTHORIZATION_HEADER, timeout=10,
    #                          verify=False).json()
    # except requests.exceptions.ReadTimeout:
    #     data = None
    # if data.get('code', 0) == 200:
    #     logger.info('Product: {0} marked for review.'.format(styfi_product_id))
    # else:
    #     logger.warn('Product: {0} not updated from API.'.format(styfi_product_id))


def email_mark_for_review():
    subject = 'Products for Review: {0}'.format(datetime.now().strftime("%d-%m-%Y"))
    body = '''
        <h3>Mark for Review</h3>
        <table style="width:100%;text-align: left;border: 1px solid black;">
          <tr>
            <th style="border: 1px solid black;"></th>
            <th style="border: 1px solid black;">Brand</th>
            <th style="border: 1px solid black;">Reason</th>
            <th style="border: 1px solid black;">Product Count</th>
          </tr>
          '''
    for key, value in brand_counter.iteritems():
        for review_key, review_reason in value.iteritems():
            body += '''<tr>
                <td style="border: 1px solid black;">''' + str(key) + '''</td>
                <td style="border: 1px solid black;">''' + str(review_key) + '''</td>
                <td style="border: 1px solid black;">''' + str(review_reasons[review_key]) + '''</td>
                <td style="border: 1px solid black;">''' + str(review_reason) + '''</td></tr>'''
    body += '''
        </table>'''
    process = subprocess.Popen(['mutt', '-e', 'set copy=no', '-e', 'set content_type=text/html', '-s', subject,
                                settings.RECIPIENT],
                               stdin=subprocess.PIPE)
    process.communicate(body)


try:
    api_url = settings.API_URL + "/product/get_elastic_search_products"
    headers = settings.AUTHORIZATION_HEADER
    params = {"page": 1, "limit": 100}
    if 'status' in args:
        status_id = args['status']
        if status_id:
            params['status'] = status_id
    if 'store' in args:
        store_id = args['store']
        if store_id:
            params['store_id'] = store_id
    if 'brand' in args:
        brand = args['brand']
        if brand:
            params['manufacturer_id'] = brand
    if 'arguments' in args:
        arguments = args['arguments']
        if arguments:
            try:
                arguments = [int(x) for x in arguments.split(',')]
            except (ValueError, TypeError):
                logger.warn('Invalid arguments passed.')
    else:
        arguments = review_reasons.keys()
    store_ids = get_stores()
    assert isinstance(store_ids, list), "Store API sent empty response."

    while True:
        logger.info('Page No: {0}'.format(params['page']))
        logger.debug(' [X] Starting call to API.')
        start_api_time = datetime.now()
        try:
            response = requests.get(api_url, headers=headers, params=params, timeout=200).json()
            end_api_time = datetime.now()
            api_diff = end_api_time - start_api_time
            logger.debug(' [X] Time taken by API: {0} secs'.format(api_diff.seconds))
            if not response.get("data", None):
                logger.warn('No product data received from API.')
                break
            for each_product in response["data"]:
                logger.debug(each_product)
                logger.debug(' [XX] Starting call to Check Product function.')
                start_function_time = datetime.now()
                product_status = check_product(each_product)
                end_function_time = datetime.now()
                function_diff = end_function_time - start_function_time
                logger.debug(' [XX] Time taken by Function: {0} secs'.format(function_diff.seconds))
                if not product_status['Passed']:
                    logger.info('STYFI PRODUCT ID: {0} STATUS: {1}'.format(each_product.get('styfi_product_id', None),
                                                                           product_status['reason']))
                # if not product_status['Passed']:
                update_status(each_product, product_status)
        except Exception as e:
            logger.error('No response from API.')
        params['page'] += 1

    email_mark_for_review()
except Exception as e:
    logger.error('Script failed because of reason: \n')
    logger.error(traceback.format_exc())
finally:
    logger.info(' [x] Process complete.')
    if os.path.isfile(pidfile):
        os.unlink(pidfile)



