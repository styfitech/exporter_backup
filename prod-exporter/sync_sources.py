import requests
from pymongo import MongoClient
uri = 'mongodb://styfiadmin:tGaKMCEZyxBK3Cad@styfimongodbclusterone-shard-00-00-1tkzd.mongodb.net:27017,styfimongodbclusterone-shard-00-01-1tkzd.mongodb.net:27017,styfimongodbclusterone-shard-00-02-1tkzd.mongodb.net:27017/<DATABASE>?ssl=true&replicaSet=styfimongodbclusterone-shard-0&authSource=admin'

client = MongoClient(uri)
db = client["styfiv2"]
collection = db["product_feed_data_27_Jun_2017"]


def add_to_mongo(product_data, collection_name):
    try:
        inserted_data = db[collection_name].insert_one(product_data)
        if inserted_data.inserted_id:
            print('Data inserted for product {0}'.format(product_data['user']['product_id']))
    except Exception as e:
        print(e)


def add_to_reason(product_data, collection_name):
    try:
        db[collection_name].insert_one(product_data)
    except Exception as e:
        print(e)


def check_product(mysql_product, mongo_product, page):
    similar = True
    print(page)
    reason = ''
    mongo_category = mongo_product['category_ids'][0]
    mysql_category = int(mysql_product['category_id'])
    if mongo_category:
        mongo_category = int(mongo_category)
    if mongo_category != mysql_category:
        mongo_product['category_ids'].append(mysql_category)
        mongo_product['category_names'].append(mysql_product['categories'][-1]['name'])
        similar = False
        reason = 'category mismatch'
        add_to_reason({'reason': reason, 'product_id': mongo_product['user']['product_id']}, "wrong_ids2")
    mongo_manufacturer_id = mongo_product['manufacturer_details']['manufacturer_id']
    mysql_manufacturer_id = mysql_product['manufacturer_id']
    if mongo_manufacturer_id != mysql_manufacturer_id:
        mongo_product['manufacturer_details']['manufacturer_id'] = mysql_manufacturer_id
        mongo_product['manufacturer_details']['manufacturer_name'] = mysql_product['manufacturer_name']
        similar = False
        reason = 'category mismatch'
        add_to_reason({'reason': reason, 'product_id': mongo_product['user']['product_id']}, "wrong_ids2")
    mysql_store = mysql_product['stores']
    mongo_store = mongo_product['store_data']
    try:
        mysql_store_id = int(mysql_store[0]['store_id'])
        mongo_store_id = int(mongo_store[0]['store_id'])
        if mysql_store_id != mongo_store_id:
            mongo_product['store_data'][0]['store_id'] = int(mysql_store_id)
            similar = False
            reason = "Store ID mismatch"
            add_to_reason({'reason': reason, 'product_id': mongo_product['user']['product_id']}, "wrong_ids2")
        mysql_store_sku = mysql_store[0]['sku']
        mongo_store_sku = mongo_store[0]['sku']
        mongo_product['store_sku'] = mysql_store_sku
        mongo_product['store_product_id'] = mysql_store_sku
        if mysql_store_sku != str(mongo_store_sku):
            mongo_product['store_data'][0]['sku'] = mysql_store_sku
            similar = False
            reason = "Store SKU mismatch"
            add_to_reason({'reason': reason, 'product_id': mongo_product['user']['product_id']}, "wrong_ids2")
        mysql_store_url = mysql_store[0]['product_url']
        try:
            mongo_store_url = mongo_store[0]['product_url']
            mongo_product['store_data'][0]['url'] = mongo_store_url
        except KeyError:
            mongo_store_url = mongo_store[0]['url']
        if mysql_store_url != mongo_store_url:
            mongo_product['store_data'][0]['url'] = mysql_store_url
            similar = False
            reason = "URL mismatch"
            add_to_reason({'reason': reason, 'product_id': mongo_product['user']['product_id']}, "wrong_ids")
        styfi_product_id = 'STYFI-{0}-{1}-{2}-{3}'.format(mysql_manufacturer_id,
                                                          mysql_store_id,
                                                          1,
                                                          mysql_store_sku
                                                          )
        mongo_product['styfi_product_id'] = styfi_product_id
        try:
            mongo_product['inspires'] = mysql_product['inspires']
            mongo_product['styfi_collection'] = mysql_product['styfi_collection']
        except Exception as e:
            pass
        try:
            mongo_product['store_data'][0]["price"] = mysql_product['stores'][0]['price']
            mongo_product['store_data'][0]["original_price"] = mysql_product['stores'][0]['original_price']
        except TypeError:
            pass
        try:
            filter_id = [filter_data['filter_id'] for filter_data in mysql_product['filters']]
            mongo_product['filter_ids'] = filter_id
            filter_names = [filter_data['name'] for filter_data in mysql_product['filters']]
            mongo_product['filter_names'] = filter_names
        except Exception as e:
            pass
    except Exception as e:
        return None
    return {'similar': similar, 'reason': reason, 'mongo_product': mongo_product}

api_url = "http://prod.styfi.in/admin_api/product/get_elastic_search_offline_products"
headers = {"authorization": "bearer hi1s3K6FrjZdUAUllXU6MCKve1xukvs5"}
params = {"page": 1, "limit": 100}
while True:
    params['page'] += 1
    response = requests.get(api_url, headers=headers, params=params).json()
    if not response["data"]:
        break
    for product in response["data"]:
        string_product_id = product['product_id']
        product_id = int(string_product_id.replace('off-', ''))
        product_mongo = collection.find({'user.product_id': product_id})
        for data in product_mongo:
            if 'variants' in data:
                checked_data = check_product(product, data, params['page'])
                if checked_data:
                    similarity = checked_data['similar']
                    checked_product = checked_data['mongo_product']
                    try:
                        db["product_feed_sync_prod"].insert_one(checked_product)
                    except Exception as e:
                        pass
                else:
                    try:
                        db["missing_store"].insert_one(checked_product)
                    except Exception as e:
                        pass
            else:
                try:
                    db["invalid_data2"].insert_one(data)
                except Exception as e:
                    pass







