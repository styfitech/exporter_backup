from datetime import datetime, timedelta
from pymongo import MongoClient
import settings
import subprocess

yesterday = datetime.now() - timedelta(days=1)

subject = "Price drop for %s" % yesterday.strftime('%d-%m-%y')

database = MongoClient(settings.STYFI_MONGODB)['styfiv2']

pipeline = [
    {
        "$match": {"data.date": {"$gt": str(yesterday)}}
    },
    {
        "$project": {
            "data": {
                "$filter": {
                    "input": "$data",
                    "as": "o",
                    "cond": {
                        "$gt": ['$$o.date', str(yesterday)]
                    }
                }
            }
        }
    }
]

aggregate_response = database['sl_product_price_drop_report'].aggregate(pipeline=pipeline, allowDiskUse=True)

body = '''
    <h3>Price Drop</h3>
    <table style="width:100%;text-align: left;border: 1px solid black;">
      <tr>
        <th style="border: 1px solid black;">Url</th>
        <th style="border: 1px solid black;">Previous Price</th>
        <th style="border: 1px solid black;">New Discounted Price</th>
        <th style="border: 1px solid black;">Original Price</th>
        <th style="border: 1px solid black;">Date</th>
        <th style="border: 1px solid black;">Sku</th>
      </tr>
      '''

reduced_price = []
for stats in aggregate_response:
    len_result = len(stats['data'])
    for i in range(0, len_result-1):
        product_data = {}
        if stats['data'][i]['is_drop']:
            product_data['previous_price'] = stats['data'][i]['previous_price']
            product_data['price'] = stats['data'][i]['store'][0]['price']
            product_data['selling_price'] = stats['data'][i]['store'][0]['original_price']
            product_data['url'] = stats['data'][i]['store'][0]['url']
            product_data['date_dropped'] = stats['data'][i]['date']
            product_data['sku'] = stats['data'][i]['store'][0]['sku']
            reduced_price.append(product_data)

for product in reduced_price:
    body += '''<tr><td style="border: 1px solid black;">''' + str(product['url']) + '''</td>
        <td style="border: 1px solid black;">''' + str(product['previous_price']) + '''</td>
        <td style="border: 1px solid black;">''' + str(product['price']) + '''</td>
        <td style="border: 1px solid black;">''' + str(product['selling_price']) + '''</td>
        <td style="border: 1px solid black;">''' + str(product['date_dropped']) + '''</td>
        <td style="border: 1px solid black;">''' + str(product['sku']) + '''</td>
        </tr>'''

body += '''
        </table>'''

process = subprocess.Popen(['mutt', '-e', 'set copy=no', '-e', 'set content_type=text/html', '-s', subject,
                            settings.PRICE_RECEIPIENT],
                           stdin=subprocess.PIPE)
process.communicate(body)
