import requests
import json
import psutil
import settings
import subprocess

logger = settings.setup_custom_logger('root')


def get_process_status(process_name, allowed_status=["running"]):
    process_on = False
    state = 'stopped'
    for running_process in psutil.process_iter():
        if process_name in running_process.name().lower():
            logger.info('Process found %s' % process_name)
            parent_process_id = running_process.ppid()
            process_status = running_process.status()
            parent = psutil.Process(parent_process_id)
            parent_process_status = parent.status()
            if process_status in allowed_status:
                logger.info('Process running.')
                process_on = True
            if parent_process_status in allowed_status:
                logger.info('Process running.')
                process_on = True
            else:
                state = parent_process_status

    return {'state': state, 'status': process_on}


def restart_service(name,command=None):
    if not command:
        command = ['/sbin/service', name, 'restart']
    try:
        message = subprocess.check_output(command, shell=False)
    except subprocess.CalledProcessError, e:
        message = e
    return message


def send_slack_notification(message):
    send_request = requests.post(settings.SLACK_URL,
                                 data=json.dumps({
                                    "channel": "#services",
                                    "username": "vaulstein",
                                    "text": message,
                                    "icon_emoji": ":hackerman:",
                                    "mrkdwn": True,
                                    "link_names": 1
                                 }))
    if send_request.ok:
        logger.info('Request Sent.')

for key, value in settings.PROCESS_TO_CHECK.items():
    status_response = get_process_status(value['service_name'], value['allowed_states'])
    status = status_response['status']
    current_state = status_response['state']
    if not status:
        logger.warn("Service {0} not running. State {1}".format(key, current_state))
        send_slack_notification("@channel Service *'{0}'* needs your attention. Current state of Process: _'{1}'_".format(key,
                                current_state))
        restart_message = restart_service(value['name'],value['command'])
        send_slack_notification("Trying to restart service. ```{0}```".format(restart_message))







