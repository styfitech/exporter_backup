from __future__ import print_function
import elasticsearch
import elasticsearch.helpers
import elasticsearch.client
import pymysql
import requests
import argparse
import json
import config

parser = argparse.ArgumentParser()
parser.add_argument("-a", "--all", help="import these data: categories, brands, tags, products", action="store_true")
parser.add_argument("-b", "--brands", help="import all brands", action="store_true")
parser.add_argument("-c", "--categories", help="import all categories", action="store_true")
parser.add_argument("-p", "--products", help="import all products", action="store_true")
parser.add_argument("-f", "--products_full", help="import all products full", action="store_true")
parser.add_argument("-t", "--tags", help="import all tags", action="store_true")
parser.add_argument("-u", "--user_scores", help="import all user_scores", action="store", default=None)
parser.add_argument("-i", '--collections', help="import all collection data", action="store_true")
args = parser.parse_args()

sql_client = pymysql.connect(host=config.DB_CREDS['host'], user=config.DB_CREDS['user'],
                             password=config.DB_CREDS['password'], port=config.DB_CREDS['port'],
                             database=config.DB_CREDS['db'],
                             cursorclass=pymysql.cursors.DictCursor)

elastic = elasticsearch.Elasticsearch(config.ELASTIC_URL)

indices = elasticsearch.client.IndicesClient(elastic)
headers = config.AUTH_HEADERS
if args.all:
    print("all arguments enabled")
    args.brands = True
    args.categories = True
    args.products = True
    args.tags = True
    args.collections = True

if args.brands:
    print("brands enabled")
    if not indices.exists(config.DB_PREFIX + "brands"):
        indices.create(config.DB_PREFIX + 'brands', body={
            "settings": {
                "analysis": {
                    "analyzer": {
                        "autocomplete": {
                            "tokenizer": "autocomplete",
                            "filter": [
                                "lowercase"
                            ]
                        },
                        "autocomplete_search": {
                            "tokenizer": "lowercase"
                        }
                    },
                    "tokenizer": {
                        "autocomplete": {
                            "type": "edge_ngram",
                            "min_gram": 2,
                            "max_gram": 10,
                            "token_chars": [
                                "letter"
                            ]
                        }
                    }
                }
            },
            "mappings": {
                "brand": {
                    "properties": {
                        "name": {
                            "type": "string",
                            "analyzer": "autocomplete",
                            "search_analyzer": "autocomplete_search"
                        }
                    }
                }
            }
        })

    with sql_client.cursor() as cursor:
        sql = """SELECT * FROM oc_manufacturer"""
        cursor.execute(sql)
        results = cursor.fetchall()
        for result in results:
            print(result)
            elastic.index(config.DB_PREFIX + "brands", "brand", {"name": result["name"], "gender": result["gender"]},
                          id=result["manufacturer_id"])

if args.categories:
    print("categories enabled")
    if not indices.exists(config.DB_PREFIX + "categories"):
        indices.create(config.DB_PREFIX + "categories", body={
            "settings": {
                "analysis": {
                    "analyzer": {
                        "autocomplete": {
                            "tokenizer": "autocomplete",
                            "filter": [
                                "lowercase"
                            ]
                        },
                        "autocomplete_search": {
                            "tokenizer": "lowercase"
                        }
                    },
                    "tokenizer": {
                        "autocomplete": {
                            "type": "edge_ngram",
                            "min_gram": 2,
                            "max_gram": 10,
                            "token_chars": [
                                "letter"
                            ]
                        }
                    }
                }
            },
            "mappings": {
                "category": {
                    "properties": {
                        "name": {
                            "type": "string",
                            "analyzer": "autocomplete",
                            "search_analyzer": "autocomplete_search"
                        },
                        "parent": {
                            "type": "string",
                            "index": "no"
                        }
                    }
                }
            }
        })


    def recurse(category_data, parent_name):
        for zxc in category_data["children"]:
            if not zxc["children"]:
                category = {"name": zxc["name"], "parent": parent_name}
                print(category)
                elastic.index(config.DB_PREFIX + "categories", "category", category, id=zxc["category_id"])
            else:
                zxc_name = parent_name + " > " + zxc["name"]
                recurse(zxc, zxc_name)


    api_url = config.STYFI_URL + "/api/product/category/tree"
    category_response = requests.get(api_url, headers=headers).json()
    data1 = category_response["data"]

    for x in data1:
        name = x["name"]
        recurse(x, name)

if args.tags:
    print("tags enabled")
    if not indices.exists(config.DB_PREFIX + "tags"):
        with sql_client.cursor() as cursor:
            for tag_group in [1, 3, 5]:
                sql = """SELECT DISTINCT ofg.`name` AS filter_group, of.filter_id AS filter_id, ofd.`name` AS filter_name
                    FROM
                    `oc_filter` AS of,
                    `oc_filter_description` AS ofd,
                    `oc_filter_group_description` AS ofg
                    WHERE
                    of.filter_id = ofd.`filter_id` AND
                    of.`filter_group_id` = ofg.`filter_group_id` AND
                    ofg.`filter_group_id` IN (1,3,5)
                """
                cursor.execute(sql)
                for result in cursor.fetchall():
                    elastic.index(config.DB_PREFIX + "tags", "tag", result, id=result["filter_id"])
                    try:
                        sql_fetch_synonym = """SELECT t2.synonym_name FROM {0}.oc_filter_to_synonym as t1
                                            LEFT OUTER JOIN {0}.sl_synonym as t2
                                            ON t1.synonym_id=t2.synonym_id WHERE filter_id={1};""".format(
                            config.DB_CREDS['db'], result["filter_id"])
                        cursor.execute(sql_fetch_synonym)
                        synonym_array = [a['synonym_name'] for a in list(cursor.fetchall())]
                        try:
                            result = elastic.update(index=config.DB_PREFIX + 'tags', doc_type='tag', id=result["filter_id"],
                                                    body={"doc": {"synonym": synonym_array}})
                        except Exception as e:
                            print('Tag not present.')
                    except Exception as e:
                        print('Missing synonyms.')

if args.products:
    print("products enabled")
    api_url = config.STYFI_URL + "/admin_api/product/get_elastic_search_products"
    headers = config.ADMIN_AUTH_HEADERS
    params = {"page": 1, "limit": 100}
    response = requests.get(api_url, headers=headers, params=params).json()

    if not indices.exists(config.DB_PREFIX + "products"):
        indices.create(config.DB_PREFIX + "products", body={
            "settings": {
                "analysis": {
                    "analyzer": {
                        "autocomplete": {
                            "tokenizer": "autocomplete",
                            "filter": [
                                "lowercase"
                            ]
                        },
                        "autocomplete_search": {
                            "tokenizer": "lowercase"
                        }
                    },
                    "tokenizer": {
                        "autocomplete": {
                            "type": "edge_ngram",
                            "min_gram": 2,
                            "max_gram": 10,
                            "token_chars": [
                                "letter"
                            ]
                        }
                    }
                }
            },
            "mappings": {
                "product": {
                    "properties": {
                        "color": {
                            "properties": {
                                "hex": {
                                    "type": "string"
                                },
                                "l": {
                                    "type": "double"
                                },
                                "location": {
                                    "type": "geo_point"
                                }
                            }
                        },
                        "categories": {
                            #"type": "nested",
                            "properties": {
                                "category_id": {
                                    "type": "short"
                                },
                                "level": {
                                    "type": "short"
                                },
                                "name": {
                                    "type": "string"
                                }
                            }
                        },
                        "date_added": {
                            "type": "date",
                            "format": "yyy-MM-dd HH:mm:ss"
                        },
                        "date_modified": {
                            "type": "date",
                            "format": "yyy-MM-dd HH:mm:ss"
                        },
                        "filters": {
                            "type": "nested",
                            "properties": {
                                "filter_id": {
                                    "type": "integer"
                                },
                                "name": {
                                    "type": "string"
                                }
                            }
                        },
                        "styfi_collection": {
                                    #"type": "nested",
                                    "properties": {
                                        "styfi_collection_id": {
                                            "type": "integer"
                                        },
                                        "sort_order": {
                                            "type": "integer"
                                        }
                                    }
                                },
                        "inspires": {
                            #"type": "nested",
                                    "properties": {
                                        "inspire_id": {
                                            "type": "integer"
                                        },
                                        "sort_order": {
                                            "type": "integer"
                                        }
                                    }
                                },
                        "manufacturer_id": {
                            "type": "integer"
                        },
                        "manufacturer_name": {
                            "type": "string",
                        },
                        "name": {
                            "type": "string",
                            "analyzer": "autocomplete",
                            "search_analyzer": "autocomplete_search"
                        },
                        "description": {
                             "type": "string",
                        },
                        "assigned_to": {
                            "type": "integer"
                        },
                        "tagged_by": {
                             "type": "integer"
                        },
                        "tagged": {
                             "type": "integer"
                        },
                        "is_offline": {
                            "type": "integer"
                        },
                        "product_id": {
                            "type": "string"
                        },
                        "styfi_product_id": {
                            "type": "string"
                        },
                        "size_available": {
                            "type": "string"
                        },
                        "quantity": {
                            "type": "integer"
                        },
                        "status": {
                            "type": "integer"
                        },
                        "stores": {
                            #"type": "nested",
                            "properties": {
                                "original_price": {
                                    "type": "float"
                                },
                                "price": {
                                    "type": "float"
                                },
                                "sku": {
                                    "type": "string"
                                },
                                "store_id": {
                                    "type": "short"
                                },
                                "store_name": {
                                    "type": "string"
                                },
                                "url": {
                                    "type": "string",
                                    "index": "no"
                                }
                            }
                        },
                        "tag": {
                            "type": "string"
                        }
                    }
                }
            }
        })


    def send_products():
        global response
        while response["data"]:
            print(response["data"])
            print("Page number: %s" % params["page"])
            for product in response["data"]:
                print(product)
                print(product["product_id"])
                if product["date_modified"] == "0000-00-00 00:00:00":
                    del product["date_modified"]

                yield {'_op_type': "index", '_index': config.DB_PREFIX + 'products', '_type': 'product', '_id': product["product_id"],
                       '_source': product}
                # elastic.index(config.DB_PREFIX + "products", "product", product, product["product_id"])
            params["page"] += 1
            response = requests.get(api_url, headers=headers, params=params).json()


    success, failed = 0, 0
    errors = []
    try:
        for x, y in elasticsearch.helpers.streaming_bulk(elastic, send_products()):
            if not x:
                print(y)
                errors.append(y)
                failed += 1
            else:
                success += 1
    except Exception as e:
        print(e)
    # except simplejson.scanner.JSONDecodeError as e:
    #     print(e)

    print(success, failed)


if args.user_scores:
    print("user scores enabled")
    print(args.user_scores)
    if not indices.exists(config.DB_PREFIX + 'user_scores'):
        indices.create(config.DB_PREFIX + "user_scores", body={
            "mappings": {
                "score": {
                    "properties": {
                        "category": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "firstname": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "lastname": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "brand": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "tag": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "gender": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "manufacturer": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "manufacturer_id": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "category_id": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "score": {
                            "type": "float",
                            "index": "not_analyzed"
                        }
                    }
                }
            }
        })

    with sql_client.cursor() as cursor:
        sql = """SELECT
  scs.score_id AS id,
  scs.customer_id,
  oc.gender,
  scs.manufacturer_id,
  scs.category_id,
  scs.filter_id AS tag_id,
  ocf.filter_group_id AS tag_group,
  scs.score,
  scs.manufacturer_score,
  scs.category_score,
  (SELECT group_concat(path_id) FROM oc_category_path WHERE category_id = scs.category_id GROUP BY category_id) AS categories
FROM
  sl_customer_score AS scs,
  oc_customer AS oc,
  oc_filter AS ocf
WHERE
  scs.customer_id = oc.customer_id AND
  scs.filter_id = ocf.filter_id AND
  scs.score_id > 2749357
"""
        if int(args.user_scores) > 0:
            sql += " AND scs.customer_id=%s" % args.user_scores

        cursor.execute(sql)


        def send_data():
            for my_object in cursor.fetchall():
                print(my_object['id'])
                if my_object['categories']:
                    my_object["categories"] = my_object["categories"].split(",")
                else:
                    my_object['categories'] = 0
                try:
                    data = {'_op_type': "index", '_index': config.DB_PREFIX + 'user_scores',
                            '_type': 'score', '_id': my_object["id"],
                            '_source': json.dumps(my_object, encoding='utf-8')}
                except Exception as e:
                    data = {'_op_type': "index", '_index': config.DB_PREFIX + 'user_scores',
                            '_type': 'score', '_id': my_object["id"],
                            '_source': json.dumps(my_object, encoding='latin-1')}
                    print(data)
                    print(e)
                    # raise Exception
                yield data


        success, failed = 0, 0
        errors = []

        for ok, item in elasticsearch.helpers.streaming_bulk(elastic, send_data()):
            if not ok:
                errors.append(item)
                failed += 1
            else:
                success += 1

        print(success, failed)

if args.collections:
    print("collections enabled")
    if not indices.exists(config.DB_PREFIX + "styfi_collection"):
        with sql_client.cursor() as cursor:
            sql = """SELECT sc.collection_id, sc.collection_name, sc.collection_image, sc.seo_url, sc.meta_title,
            sc.meta_description, sc.collection_type, sc.gender, sc.date_modified,
            sc.view_count, t2.tag_id, type FROM {0}.sl_styfi_collection as sc
            LEFT OUTER JOIN {0}.sl_new_tags_detail as t2
            ON sc.collection_id=t2.foreign_id WHERE sc.collection_status = 'enable';""".format(config.DB_CREDS['db'])
            cursor.execute(sql)
            for result in cursor.fetchall():
                collection_id = result['collection_id']
                result.pop('collection_id')
                if result['meta_title']:
                    result['meta_title'] = result['meta_title'].decode('utf-8', 'ignore')
                result['name'] = result['collection_name'].decode('utf-8', 'ignore')
                result.pop('collection_name')
                elastic.index(config.DB_PREFIX + "collections", "collection", result, id=collection_id)

