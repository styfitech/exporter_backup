#!/usr/bin/env python
import os
import sys
import pika
import time
import json
import settings
from pymongo import MongoClient, IndexModel, DESCENDING
import requests
import datetime
import math
import string
import random
import re
import signal

# Checking if process is already running, if it is not, then starting
pid = str(os.getpid())
pidfile = "/tmp/fast_extract.pid"

logger = settings.setup_custom_logger('root')

if os.path.isfile(pidfile):
    logger.warn('%s already exists, exiting' % pidfile)
    sys.exit()

open(pidfile, 'w').write(pid)
# Signal handler to clean resources on shutdown


def handler(signum, frame):
    logger.info('Received kill signal. Shutting Down.')
    os.unlink(pidfile)
    sys.exit(1)

signal.signal(signal.SIGTERM, handler)

try:

    store_data = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION][settings.PRODUCT_COLLECTION]
    api_data = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION][settings.API_COLLECTION]

    if settings.credentials:
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port'], credentials=settings.credentials))
    else:
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port']))
    channel = connection.channel()

    channel.queue_declare(queue='live:items', durable=True, arguments={'x-max-priority': 10})
    channel.basic_qos(prefetch_count=1)
    logger.info(' [*] Waiting for messages. To exit press CTrL+C')


    def unique_id_generator(prefix='', more_entropy=False):
        m = time.time()
        unique_id = '%8x%05x' % (math.floor(m), (m - math.floor(m)) * 1000000)
        if more_entropy:
            valid_chars = list(set(string.hexdigits.lower()))
            entropy_string = ''
            for i in range(0, 10, 1):
                entropy_string += random.choice(valid_chars)
            unique_id = unique_id + entropy_string
        unique_id = prefix + unique_id
        return unique_id


    def callback(ch, method, properties, body):
        headers = {
            'User-Agent':
                'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
        }

        dummy_product_data = {"product_description":
            {"1":
                {
                    "name": "",
                    "description": "",
                    "meta_title": "Product Meta",
                    "meta_description": "",
                    "meta_keyword": "",
                    "tag": ""
                }
            },
            "model": "",
            "sku": "",
            "upc": "",
            "ean": "",
            "jan": "",
            "isbn": "",
            "mpn": "",
            "location": "",
            "price": "",
            "tax_class_id": "",
            "quantity": "",
            "minimum": "",
            "subtract": "",
            "stock_status_id": "",
            "shipping": "",
            "keyword": "",
            "date_available": "",
            "length": "",
            "width": "",
            "height": "",
            "length_class_id": "",
            "weight": "",
            "weight_class_id": "",
            "status": "",
            "sort_order": "",
            "manufacturer": "",
            "manufacturer_id": "",
            "category": "",
            "product_category": [],
            "filter": "",
            "product_filter": [],
            "product_store": [
                {"store_id": "",
                 "price": "",
                 "original_price": "",
                 "url": "",
                 "discount": ""
                 }
            ],
            "download": "",
            "related": "",
            "option": "",
            "image": [],
            "points": "",
            "product_reward": {"1": {"points": ""}},
            "product_layout": ["", "", ""]
        }

        message = ''
        logger.info(' [x] Received %r' % body)
        product = json.loads(body)
        final_product_data = dummy_product_data.copy()

        assert isinstance(product, dict)
        test = product.copy()
        categorization_product = product.copy()
        assert isinstance(test, dict)

        final_product = {}

        # Removing attributes not required for processing
        for key_delete in [
            "_id", "url", "price", "name", "store", "category_id", "brand_id", "images", "category_id", "discount",
            'extract_time', "breadcrumbs", "styfi_category", 'mapped', 'new_data', "og_url", "features", "sizes",
            "discount_percent"
        ]:
            test.pop(key_delete, None)

        product.pop('new_data', None)

        if product['store'] == 'yepme' or product['store'] == 'koovs' or product['store'] == 'flipkart':
            pass
        elif product['store'] == 'amazon' and 'ref=as_li_tl' not in product['url']:
            try:
                if '/ref' in product['url']:
                    product['url'] = re.sub(r"/ref=\w+", '/ref=as_li_tl?ie=UTF8&tag=wwwstyfiin-21', product['url'])
                else:
                    product['url'] = product['url'].split('?')[0] + '/ref=as_li_tl?ie=UTF8&tag=wwwstyfiin-21'
            except:
                product['url'] = product['url'].split('?')[0] + '/ref=as_li_tl?ie=UTF8&tag=wwwstyfiin-21'
            if 'qid' in product and product['qid']:
                product['url'] += '&' + product['qid']
        else:
            if '?' in product['url']:
                product['url'] = product['url'].split('?')[0]
        logger.info(product['url'])
        if 'sku' not in test:
            previous_saved = store_data.find_one({'url': product['url']})
            if previous_saved:
                if 'sku' in previous_saved:
                    product['sku'] = previous_saved['sku']
                else:
                    product['sku'] = unique_id_generator()
                if 'assign_to' in previous_saved:
                    final_product_data['stylist'] = previous_saved['assign_to']
        else:
            test.pop("sku")

        if 'sold_out' in test:
            test.pop('sold_out')
            final_product_data['quantity'] = 0
            final_product_data['status'] = 0
            logger.info('Status set to 0 for url: %s' % product['url'])
        elif 'discontinue' in test:
            final_product_data['status'] = -1
            logger.info('Product Discontinued.')
        else:
            final_product_data['status'] = 1
        if 'status' in product and product['status'] == -1:
            final_product_data['status'] = -1

        if 'assign_to' in product:
            final_product_data['stylist'] = test['assign_to']

        if 'color' in test:
            product['color'] = [test['color']]
        if 'Color' in test:
            product['color'] = [test['Color']]

        if 'original_price' in test:
            final_product["original_price"] = test['original_price']
            test.pop("original_price")
        if 'before_price' in test:
            final_product["original_price"] = test['before_price']
            test.pop("before_price")
        if 'original_price' not in final_product and 'price' in product:
            final_product['original_price'] = product['price']
        if 'price' in product:
            if product['price'] == 0:
                final_product_data['status'] = -2
            final_product['price'] = product['price']
            if final_product['price'] > final_product['original_price']:
                final_product['original_price'] = final_product['price']
        else:
            final_product_data['status'] = 0

        if 'extract_time' in categorization_product:
            categorization_product.pop('extract_time')

        if 'sizes' in product:
            final_product_data['sizes'] = product['sizes']
        if 'sku' in product:
            final_product_data['sku'] = product['sku']
        if 'price' in final_product:
            final_product_data['price'] = final_product['price']
            final_product_data['product_store'][0]['price'] = final_product['price']
        else:
            if 'original_price' in final_product:
                final_product_data['price'] = final_product['original_price']
                final_product_data['product_store'][0]['price'] = final_product['original_price']
        if 'original_price' in final_product:
            final_product_data['product_store'][0]['original_price'] = final_product['original_price']
        if 'url' in product:
            final_product_data['product_store'][0]['url'] = product['url']
        if 'store' in product:
            final_product_data['product_store'][0]['store_name'] = product['store']
        if 'sku' in product:
            final_product_data['product_store'][0]['sku'] = product['sku']
        if 'discount' in product:
            final_product_data['product_store'][0]['discount'] = product['discount']
        else:
            if ('original_price' in final_product and 'price' in final_product and
                    isinstance(final_product['original_price'], (int, float)) and
                    final_product['original_price'] != final_product['price']):
                discount = int((final_product['original_price'] - final_product['price']) / float(
                    final_product['original_price']) * 100)
                final_product_data['product_store'][0]['discount_percent'] = discount
                final_product_data['product_store'][0]['discount'] = '( ' + str(discount) + '% OFF)'
        if 'discount_percent' in product:
            final_product_data['product_store'][0]['discount_percent'] = product['discount_percent']
        if 'extract_time' in product and isinstance(product['extract_time'], datetime.datetime):
            final_product_data['data_available'] = json.dumps(product['extract_time'].isoformat())
        if 'images' in product:
            final_product_data['image'] = product['images']

        if 'brand_id' in product:
            final_product_data['manufacturer_id'] = product['brand_id']
        if 'category_id' in product:
            final_product_data['product_category'] = [product['category_id']]

        if final_product_data['image']:
            if 'stylist' in final_product_data:
                logger.info('Stylist Id present. Assigning directly.')
                data = requests.post(settings.API_URL + '/product/make_live', data=json.dumps(final_product_data),
                                     headers=settings.AUTHORIZATION_HEADER)
                final_product_data['extract_time'] = datetime.datetime.utcnow()
                api_inserted = api_data.insert_one(final_product_data)
                if api_inserted.inserted_id:
                    logger.info('API data saved to DB.')
            else:
                data = requests.post(settings.API_URL + '/product/add_product', data=json.dumps(final_product_data),
                                     headers=settings.AUTHORIZATION_HEADER)
                final_product_data['extract_time'] = datetime.datetime.utcnow()
                api_inserted = api_data.insert_one(final_product_data)
                if api_inserted.inserted_id:
                    logger.info('API data saved to DB.')
        else:
            final_product_data['status'] = -1
            data = requests.post(settings.API_URL + '/product/add_product', data=json.dumps(final_product_data),
                                 headers=settings.AUTHORIZATION_HEADER)
            final_product_data['extract_time'] = datetime.datetime.utcnow()
            api_inserted = api_data.insert_one(final_product_data)
            if api_inserted.inserted_id:
                logger.info('API data saved to DB.')
            logger.info('Product image missing for %s' % product['url'])
        try:
            if data:
                json_response = json.loads(data.text)
                if json_response['code'] == 200:
                    product['added'] = True
                    inserted_data = store_data.insert_one(product)
                    if inserted_data.inserted_id:
                        logger.info('Product added into DB.')
                else:
                    logger.info(data.text)
                    message = 'Product not sent to API: Url - %s' % product['url']
            else:
                logger.info('Image missing.')
        except ValueError:
            message = 'Product not sent to API: Url - %s' % product['url']

        logger.info(message)
        logger.info(' [x] Item feed API call')
        ch.basic_ack(delivery_tag=method.delivery_tag)


    channel.basic_consume(callback,
                          queue='live:items')

    channel.start_consuming()
finally:
    logger.info(' [x] Closing Connection to RabbitMQ.')
    if os.path.isfile(pidfile):
        os.unlink(pidfile)
