import time
import subprocess
from datetime import datetime, timedelta
import settings

ten_days = datetime.now() - timedelta(days=10)
mongo_host = '172.31.2.103:27017'

now = int(time.time())
string_ten_days = ten_days.strftime('%Y-%m-%d')
collection_name = (settings.PRODUCT_COLLECTION_WITHOUT_TIMESTAMP + string_ten_days)
cmd = "mongodump --host " + mongo_host + " --db styfi_curated --out /home/mongodump --collection " + collection_name

cmd += ''' && mongo --eval "db=db.getSiblingDB("styfi_curated"); db.''' + collection_name + '''.drop();"'''
print subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
