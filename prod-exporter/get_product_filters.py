import requests
import logging


def setup_custom_logger(log_name):
    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger_name = logging.getLogger(log_name)
    logger_name.setLevel(logging.INFO)
    logger_name.addHandler(handler)
    return logger_name

logger = setup_custom_logger('root')

APP_HEADER = {'authorization': 'bearer hi1s3K6FrjZdUAUllXU6MCKve1xukvs5'}


def get_product_category(cate_id, limit=12):
    # logger.info('Fetching filters for category: %s' % cate_id)
    request_params = {'category_id': cate_id,
                      'limit': 100}
    page = 1
    cat_url = 'https://styfi.in/admin_api/product'
    all_product_data = []
    imageCount = 0
    while imageCount < limit:
        request_params['page'] = page
        product_data_json = requests.get(cat_url, headers=APP_HEADER, params=request_params).json()
        product_data = product_data_json['data']
        if not product_data:
            break

        for each_data in product_data:
            images_data = str(each_data['images'][0])
            filters = each_data['filters']
            filter_dict = {}
            for filte in filters:
                filter_dict[str(filte['filter_group_name'])] = [str(a['filter_id']) for a in filte['filters']]
            product_info = {'category_id': cate_id, 'filters': filter_dict, 'image_url': images_data,
            'product_id': str(each_data['product_id'])}
            all_product_data.append(product_info)
            #index, value = min(enumerate([int(a['sort_order']) for a in images_data]), key=operator.itemgetter(1))
            #download_image_folder(images_data[index]['popup'], path_name)
        page += 1
        imageCount +=100
    return all_product_data

get_product_category(160)