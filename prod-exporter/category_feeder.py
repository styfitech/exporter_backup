#!/usr/bin/env python
import os
import sys
import pika
import json
from pymongo import MongoClient
import pickle
from scrapy.http import Request
from scrapy.utils.reqser import request_to_dict
import settings
import requests
import tldextract
import argparse
from urllib import unquote
import math
import string
import random
import time

# Checking if process is already running, if it is not, then starting
pid = str(os.getpid())
pidfile = "/tmp/category_feed.pid"

logger = settings.setup_custom_logger('root')

if os.path.isfile(pidfile):
    logger.info('%s already exists, exiting' % pidfile)
    sys.exit()

open(pidfile, 'w').write(pid)


def separate_urls(list_to_be_sorted):
    sorted_list = sorted(list_to_be_sorted, key=lambda k: k['crawler_url'])
    serparated_urls = []

    for m, n in zip(sorted_list, reversed(sorted_list)):
        domain_start = tldextract.extract(m['crawler_url'])
        store_start = domain_start.domain
        domain_end = tldextract.extract(n['crawler_url'])
        store_end = domain_end.domain
        if store_start != store_end:
            serparated_urls.append(m)
            serparated_urls.append(n)
        else:
            serparated_urls.append(m)
        if len(serparated_urls) == len(sorted_list):
            break
    return serparated_urls


def unique_id(prefix='', more_entropy=False):
    m = time.time()
    unique_id = '%8x%05x' % (math.floor(m), (m - math.floor(m)) * 1000000)
    if more_entropy:
        valid_chars = list(set(string.hexdigits.lower()))
        entropy_string = ''
        for i in range(0, 10, 1):
            entropy_string += random.choice(valid_chars)
        unique_id = unique_id + entropy_string
    unique_id = prefix + unique_id
    return unique_id


try:
    category_id = None
    brand_id = None
    # If category ID is passed, sending API calls to fetch category urls else get all category urls
    parser = argparse.ArgumentParser()
    parser.add_argument('-category', nargs='?', help='pass category id')
    parser.add_argument('-priority', nargs='?', help='pass priority for crawl')
    parser.add_argument('-keyword', nargs='?', help='pass keywords present in urls to search')
    parser.add_argument('-brand', nargs='?', help='pass brand id')
    args = vars(parser.parse_args())
    if 'category' in args:
        category_id = args['category']
        logger.info('Category ID: %s' % category_id)
    if 'brand' in args:
        brand_id = args['brand']
        logger.info('Brand ID: %s' % brand_id)
    if 'priority' in args:
        priority = args['priority']
        logger.info('Priority: %s' % priority)
    if not priority:
        priority = 0
        logger.info('Setting normal priority.')
    if 'keyword' in args:
        keyword = args['keyword']
        logger.info('Keyword to be used to filter urls: %s' % keyword)

    request_params = {}
    if category_id:
        request_params['category_id'] = category_id
    if brand_id:
        request_params['manufacturer_id'] = brand_id
    data_category = requests.get(settings.API_URL + '/crawler', headers=settings.AUTHORIZATION_HEADER,
                                 params=request_params)

    store_data = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION][settings.CATEGORY_COLLECTION]
    data_dict = json.loads(data_category.text)
    crawling_data_urls = data_dict['data']
    separate_urls_data = separate_urls(crawling_data_urls)

    routing_key_value = 'products:requests'
    # Adding urls to RabbitMq for processing by the crawlers
    if settings.credentials:
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port'], credentials=settings.credentials))
    else:
        connection = pika.BlockingConnection(pika.ConnectionParameters(
                        host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port']))

    channel = connection.channel()

    for product in separate_urls_data:
        # Fetching Domain name from Urls
        headers = {
            'User-Agent':
                random.choice(settings.USER_AGENTS)
        }
        domain_data = tldextract.extract(product['crawler_url'])
        store = domain_data.domain

        if 'styfi_category' not in product:
            product['styfi_category'] = None
        if 'filter_id' not in product:
            product['filter_id'] = None

        if store and (not keyword or keyword in product['crawler_url']):
            product['crawler_url'] = unquote(product['crawler_url'].replace('&amp;', '&'))
            # Handling Redirects for Koovs
            if store == 'koovs':
                req = Request(product['crawler_url'],
                              callback="parse",
                              meta={'styfi_category': product['styfi_category'],
                                    'store': store,
                                    'category_id': product['category_id'],
                                    'brand_id': product['manufacturer_id'],
                                    'product_filter': product['filter_id']},
                              dont_filter=True,
                              headers=headers)
            else:
                if store == 'limeroad':
                    product['crawler_url'] = product['crawler_url'].replace('#', '?')
                req = Request(product['crawler_url'],
                              callback="parse",
                              meta={'styfi_category': product['styfi_category'],
                                    'store': store,
                                    'category_id': product['category_id'],
                                    'brand_id': product['manufacturer_id'],
                                    'product_filter': product['filter_id']
                                    },
                              dont_filter=True,
                              headers=headers)
            msg = pickle.dumps(request_to_dict(req), protocol=-1)
            channel.basic_publish(exchange='',
                                  routing_key=routing_key_value,
                                  body=msg,
                                  properties=pika.BasicProperties(
                                      priority=priority
                                  ))
            try:
                inserted_data = store_data.insert_one(product)
            except Exception as e:
                product['_id'] = unique_id()
                inserted_data = store_data.insert_one(product)
            if inserted_data.inserted_id:
                logger.info('Product with url %s added into DB.' % product['crawler_url'])
finally:
    logger.info(' [x] Closing Connection to RabbitMQ.')
    os.unlink(pidfile)
