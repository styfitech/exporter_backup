from scrapinghub import HubstorageClient
import time
import logging
import settings

hc = HubstorageClient(auth=settings.SPIDER_API_KEY)

project = hc.get_project(settings.SPIDER_ID)

jobs_metadata = project.jobq.list(state='running')

for each_running_job in jobs_metadata:
    job = hc.get_job(each_running_job['key'])
    logging.info('Stopping Job with key %s' % each_running_job['key'])
    job.request_cancel()

logging.info('Waiting for job to finish ..')
time.sleep(300)
logging.info('Checking if all jobs have ended..')
jobs_still_metadata = project.jobq.list(state='running')

for each_running_job in jobs_still_metadata:
    logging.info('Waiting some more time for job to end')
    time.sleep(20)

logging.info('Starting new instances of details and products spiders')

job_product = project.push_job(spidername='products')
logging.info('Current state of product spider %s' % job_product.metadata['state'])

for i in range(1, 6):
    job_recrawl = project.push_job(spidername='recrawl')
    logging.info('Current state of product spider %s' % job_product.metadata['state'])

for i in range(1, 3):
    job_details = project.push_job(spidername='details', spider_args={'index': i})
    logging.info('Current state of %d product spider %s' % (i, job_product.metadata['state']))


logging.info('[X] Exiting.')
