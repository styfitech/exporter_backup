#!/usr/bin/env python
import os
# import sys
import pika
from pymongo import MongoClient
import settings
import requests
import argparse
import time, string, math, random
import json
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
import sys
import threading
import subprocess
from datetime import datetime

s = requests.Session()
error_clients = {}
retries = Retry(total=3,
                backoff_factor=10,)

#s.mount(settings.PARTNER_URL, HTTPAdapter(max_retries=retries))


# Checking if process is already running, if it is not, then starting
pid = str(os.getpid())
pid_prefix = settings.PID_PREFIX
pidfile = "/tmp/" + pid_prefix + "styfi_partners.pid"

logger = settings.setup_custom_logger('root')

parser = argparse.ArgumentParser()
parser.add_argument('-store', nargs='?', help='pass store name or store id')
parser.add_argument('-skip', nargs='?', help='skip store id')
parser.add_argument('-ids', nargs='?', help='crawl product ids directly')
parser.add_argument('-p', '--priority', type=int, nargs='?', help='set priority for crawls')
parser.add_argument('-s', '--spawn', type=int, choices=range(1, 9), nargs='?', help='spawn a thread')
parser.add_argument('-skip_store_type','--skip_store_type', nargs='?', help='skip store type like - magento,shopify')
args = vars(parser.parse_args())
if 'spawn' in args:
    spawn = args['spawn']
    if spawn:
        # max 9 threads
        pidfile = "/tmp/" + pid_prefix + "styfi_partners" + str(spawn) + ".pid"

if os.path.isfile(pidfile):
    logger.info('%s already exists, exiting' % pidfile)
    sys.exit()

open(pidfile, 'w').write(pid)

store_data = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION]['styfi_partners']

def send_mail(subject, recipient, body):
    process = subprocess.Popen(['mutt', '-e', 'set copy=no', '-e', 'set content_type=text/html', '-s',
                                subject, recipient],
                               stdin=subprocess.PIPE)
    process.communicate(body)

def send_slack(msg):
    requests.post(
        settings.SLACK_URL,
        data=json.dumps({
            "channel": "#partner_tracking", "username": "webhookbot",
            "text": msg,
            "icon_emoji": ":styfi:"}))


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    link_list = {}
    for i in xrange(0, len(l), n):
        link_list[i] = l[i:i + n]
    return link_list


def fetch_products(storeid=None, storetype=None, product_ids=None, get_ids=None, store_value=None):
    page = 1
    request_params = {}
    store_name = store_value['store_name']
    styfi_store_id = store_value['store_id']
    brand_id = store_value['brand_id']
    while True:
        if storetype == 'magento':
            if get_ids:
                request_params['get_ids'] = 1
                request_params['store'] = storeid
            else:
                if 'product_ids' in request_params:
                    break
                request_params['get_ids'] = 0
                request_params['store'] = storeid
                if isinstance(product_ids, basestring):
                    request_params['product_ids'] = product_ids
                else:
                    request_params['product_ids'] = ",".join(product_ids)
        else:
            request_params['store'] = storeid
            request_params['page'] = page
        logger.info('Fetching Products for page: {0} Store ID: {1}'.format(page, storeid))
	if 'product_ids' in request_params:
		logger.info('******Product Ids ******')
		logger.info(request_params['product_ids'])
        try:
            products_call = s.get(settings.PARTNER_URL + 'get_all_products',
                                  headers=settings.AUTHORIZATION_HEADER,
                                  params=request_params,
                                  verify=False,
                                  #timeout=6000
                                  ).json()
            if get_ids:
                logger.info('Fetching IDs for magento.')
            else:
                logger.info('Fetching Products.')
        except (ValueError, requests.exceptions.ConnectionError):
            messages = "Get all products is not working for store name: *'{0}'*".format(store_value['store_name'])
            if get_ids:
                logger.info('Failed while fetching IDs')
            else:
                logger.info('Failed while fetching Products.')
            error_clients[store_value['store_name']] = messages
            send_slack(messages)
            logger.info(messages)
            break
        page_num = products_call.get('is_last_page', None)
        if page_num:
            if int(page_num) == 1:
                break
            elif int(page_num) == -1:
                page += 1
                continue
        products_data = products_call.get('data', None)
        if storetype == 'magento' and get_ids:
            try:
                product_id_dict = chunks(products_data, 10)
                logger.info('Fetching products for magento.')
                for index, tw_ids in product_id_dict.items():
                    fetch_products(storeid, storetype, tw_ids, store_value=store_value)
            except TypeError:
                messages = "Get all products is not working for store name: *'{0}'*".format(store_value['store_name'])
                logger.info('Failed while fetching Products')
                error_clients[store_value['store_name']] = messages
                send_slack(messages)
                logger.info(messages)
                break
        if not products_data:
            messages = "Get all products is not working for store name: *'{0}'*".format(store_value['store_name'])
            send_slack(messages)
            error_clients[store_value['store_name']] = messages
            logger.info(messages)
            break

        routing_key_value = settings.QUEUE.get('product_details', 'details') + ':items'
        virtual_host = settings.RABBITMQ_HOST.get('vhost', '/')
        # Adding urls to RabbitMq for processing by the crawlers
        if settings.credentials:
            connection = pika.BlockingConnection(pika.ConnectionParameters(
                host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port'],
                virtual_host=virtual_host, heartbeat_interval=0, credentials=settings.credentials))
        else:
            connection = pika.BlockingConnection(pika.ConnectionParameters(
                host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port']))

        channel = connection.channel()

        for product in products_data:
            if 'is_in_stock' in product:
                stock_status = int(product['is_in_stock'])
                if 'price' in product:
                    product['styfi_partner'] = True
                    product['store'] = store_name
                    product['brand_id'] = brand_id
                    product['store_id'] = styfi_store_id
                    # Fetching Domain name from Urls
                    channel.basic_publish(exchange='',
                                          routing_key=routing_key_value,
                                          body=json.dumps(product),
                                          properties=pika.BasicProperties(
                                              priority=priority
                                          ))
                    try:
                        inserted_data = store_data.insert_one(product)
                    except Exception as e:
                        product['_id'] = unique_id_gen()
                        inserted_data = store_data.insert_one(product)
                    if inserted_data.inserted_id:
                        logger.info("Product with url '{0}' and store id '{1}' added into DB.".format(product['url'],
                                                                                                      styfi_store_id))
                else:
                    messages = "Missing product price info: '{0}'".format(product['url'])
                    send_slack(messages)
                    logger.info(messages)
            else:
                messages = "Stock status missing: : '{0}'.".format(product['url'])
                send_slack(messages)
                logger.info(messages)
        connection.close()
        page += 1


def unique_id_gen(prefix='', more_entropy=False):
    m = time.time()
    unique_id = '%8x%05x' % (math.floor(m), (m - math.floor(m)) * 1000000)
    if more_entropy:
        valid_chars = list(set(string.hexdigits.lower()))
        entropy_string = ''
        for i in range(0, 10, 1):
            entropy_string += random.choice(valid_chars)
        unique_id = unique_id + entropy_string
    unique_id = prefix + unique_id
    return unique_id

try:
    if 'skip_store_type' in args:
        skip_store_type = args['skip_store_type']
    if 'ids' in args:
        products_ids = args['ids']
    if 'skip' in args:
        skip_store_id = args['skip']
    priority = args.get('priority', 1)
    if 'store' in args:
        store_id = args['store']
        if store_id:
            store_id = int(store_id)
            logger.info('Store ID: %s' % store_id)
            # Fetching store type
            try:
                store_response = requests.get(settings.PARTNER_URL + 'get_partner_stores_minimal',
                                              headers=settings.AUTHORIZATION_HEADER,
                                              verify=False,
                                              timeout=40).json()
            except (ValueError, requests.exceptions.ConnectionError):
                message = "Get partner store *api* is not working."
                send_slack(message)
                logger.info(message)
                sys.exit(0)
            store_response_data = store_response['data']
            for store in store_response_data:
                if store['id'] == store_id:
                    store_type = store['store_type']
                    store_values = {'store_name': store['name'], 'brand_id': store['brand_id'],
                                    'store_id': store['styfi_store_id']}
                    logger.info('Store name: %s' % store['name'])
                    logger.info('Styfi store id: %s' % store['styfi_store_id'])
                    if skip_store_type:
                        if skip_store_type == store_type:
                            continue
                    if store_type == 'magento':
                        if products_ids:
                            logger.info('Fetching by Product ID')
                            fetch_products(store_id, store_type, product_ids=products_ids, store_value=store_values)
                        else:
                            fetch_products(store_id, store_type, get_ids=1, store_value=store_values)
                    else:
                        fetch_products(store_id, store_type, store_value=store_values)
                    break
        else:
            logger.info('Fetching all store data.')
            try:
                store_response = requests.get(settings.PARTNER_URL + 'get_partner_stores_minimal',
                                              headers=settings.AUTHORIZATION_HEADER,
                                              verify=False,
                                              timeout=40).json()
            except (ValueError, requests.exceptions.ConnectionError):
                message = 'Get partner store *api* is not working.'
                send_slack(message)
                logger.info(message)
                sys.exit(0)
            store_response_data = store_response['data']
            for store in store_response_data:
                store_id = store['id']
                store_type = store['store_type']
                if skip_store_id:
                    if store_id == int(skip_store_id):
                        continue
                if skip_store_type:
                    if skip_store_type == store_type:
                        continue
                store_values = {'store_name': store['name'], 'brand_id': store['brand_id'],
                                'store_id': store['styfi_store_id']}
                logger.info('Store name: %s' % store['name'])
                logger.info('Styfi store id: %s' % store['styfi_store_id'])
                if store_type == 'magento':
                    threading.Thread(target=fetch_products, args=(store_id, store_type),
                                     kwargs={'get_ids': 1, 'store_value': store_values}, name='magento_thread').start()
                else:
                    threading.Thread(target=fetch_products, args=(store_id, store_type),
                                     kwargs={'store_value': store_values}, name='normal_thread').start()
finally:
    logger.info(' [xx] Client Error Dictionary.')
    logger.info(error_clients)
    if error_clients:
	send_mail('Clients Error Report', 'shivdhwaj@stylabs.in', json.dumps(error_clients))
    logger.info(' [x] Closing Connection to RabbitMQ.')
    os.unlink(pidfile)

