import MySQLdb
import predictionio
import logging
from pymongo import MongoClient
import settings
import pytz
from datetime import datetime, timedelta
import requests

results_db = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION]['recommendation_train']
results_db_item = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION]['recommendation_train_item']
ACCESS_KEY = "GNJjQhKakqusSCE8PaYesgnao8gpExuWC13ByGzoV1eEuoL2-78vRsxmbgEz9CIF"

client = predictionio.EventClient(
    access_key=ACCESS_KEY,
    # url="http://192.168.1.200:7070",
    url="http://127.0.0.1:7070",
    threads=5,
    qsize=500
)

event_weightage = {
    'view': 1,
    'notify': 2,
    'wishlist': 3,
    'share': 1,
    'buy': 5
}

action_to_event_mapping = {
    'product_details': 'view',
    'buy_button': 'buy'
}

data_category = requests.get(settings.API_URL + '/category', headers=settings.AUTHORIZATION_HEADER).json()
category_system = data_category['data']
gender_mapping = {'Men': [], 'Women': []}
for category in category_system:
    if 'Women' in category['name']:
        gender_mapping['Women'].append(category['category_id'])
    else:
        gender_mapping['Men'].append(category['category_id'])

category_path = {}


def recurse(category_data, parent_name, parent_id, required_cat=None):
    for zxc in category_data["children"]:
        if not zxc["children"]:
            category_name = {"name": zxc["name"], "parent": parent_name, "parent_id": parent_id}
            category_name["parent"] += (" > " + zxc["name"])
            category_name["parent_id"] += (" > " + zxc["category_id"])
            category_path[zxc["category_id"]] = category_name["parent_id"]
        else:
            zxc_name = parent_name + " > " + zxc["name"]
            zxc_id = parent_id + " > " + zxc["category_id"]
            recurse(zxc, zxc_name, zxc_id, required_cat)

api_url = "http://www.styfi.in/api/product/category/tree"
category_response = requests.get(api_url, headers=settings.APP_HEADER).json()
data1 = category_response["data"]

for x in data1:
    p_name = x['name']
    p_cat = x["category_id"]
    recurse(x, p_name, p_cat)

db = MySQLdb.connect(host="styfi-rds.cbjdlzx4uat7.ap-south-1.rds.amazonaws.com",    # your host, usually localhost
                     user="styfidbadmin",         # your username
                     passwd="FQy5wlUSiC",  # your password
                     db="styfiv2")        # name of the data base

# you must create a Cursor object. It will let
#  you execute all the queries you need
cur = db.cursor()

# Use all the SQL you like

# date_start = datetime.now() - timedelta(days=60)
# print all the first cell of all the rows
for row in results_db.find({}):
    product_id = row['target_entity_id']
    cur.execute('''SELECT t3.product_id, t3.filter_id, t4.manufacturer_id, t2.category_id, t4.tagged, t4.date_added
                FROM styfiv2.oc_product_filter as t3
                LEFT OUTER JOIN styfiv2.oc_product as t4
                ON t3.product_id=t4.product_id
                INNER JOIN styfiv2.oc_product_to_category as t2
                ON t2.product_id=t4.product_id
                WHERE t2.product_id=''' +
                str(product_id) + ''';''')
    try:
        cursor_data = list(cur.fetchall())
        try:
            category = cursor_data[0][3]
            if category in gender_mapping['Women']:
                gender = ['Female']
            else:
                gender = ['Male']
            category = str(category)
            category_tree = category_path.get(category, None)
            if category_tree:
                category = category_tree.split(' > ')
            tagged_value = cursor_data[0][4]
            if tagged_value == 1:
                tagged = ['True']
            else:
                tagged = ['False']
            date_start = cursor_data[0][5]
            brand = cursor_data[0][2]
            filter_ids = [str(a[1]) for a in cursor_data]
            properties = {
                    'categories': [category],
                    'brand': [str(brand)],
                    'filters': filter_ids,
                    'gender': gender,
                    'tagged': tagged
                    #'updateDate': str(date_start.replace(tzinfo=pytz.timezone('Asia/Calcutta')))
                }
        except IndexError:
            cur.execute('''SELECT t4.product_id, t4.manufacturer_id, t2.category_id, t4.tagged, t4.date_added
            FROM styfiv2.oc_product as t4
            INNER JOIN styfiv2.oc_product_to_category as t2
            ON t2.product_id=t4.product_id
            WHERE t2.product_id=''' + str(product_id) + ''';''')
            cursor_data = list(cur.fetchall())
            category = cursor_data[0][2]
            if category in gender_mapping['Women']:
                gender = ['Female']
            else:
                gender = ['Male']
            tagged_value = cursor_data[0][3]
            if tagged_value == 1:
                tagged = ['True']
            else:
                tagged = ['False']
            date_start = cursor_data[0][4]
            brand = cursor_data[0][1]
            properties = {
                'categories': [category],
                'brand': [str(brand)],
                'gender': gender,
                'tagged': tagged
                # 'updateDate': str(date_start.replace(tzinfo=pytz.timezone('Asia/Calcutta')))
            }
        event_dict = {
            'event': "$set",
            'entity_id': product_id,
            'entity_type': "item",
            'event_time': date_start.replace(tzinfo=pytz.utc),
            'properties': properties
        }

        mongo_insert = results_db_item.insert_one(event_dict)
        if mongo_insert.inserted_id:
            logging.info('Product added into DB.')
        #logging.info('Creating an event for track id: %d' % row[0])
        # A user rates an item

        client.create_event(
            event="$set",
            entity_id=product_id,
            entity_type="item",
            properties=properties,
            event_time=date_start.replace(tzinfo=pytz.utc)
        )
    except Exception as e:
        print product_id
        print e


logging.info('terminating')
db.close()
