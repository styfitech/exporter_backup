import requests

from backup import settings


# Enter the region your instances are in, e.g. 'us-east-1'
# Enter your instances here: ex. ['X-XXXXXXXX', 'X-XXXXXXXX']

def send_slack_notification(message):
    send_request = requests.post(settings.SLACK_URL,
                                 json={
                                    "channel": "#services",
                                    "username": "vaulstein",
                                    "text": message,
                                    "icon_emoji": ":hackerman:",
                                    "mrkdwn": True
                                 })
    if send_request.ok:
        print 'Request Sent.'

# def lambda_handler(event, context):
# 	region = event['ap-south-1']
# 	instances = event["detail"]["instance-id"]
#     ec2 = boto3.client('ec2', region_name=region)
#
#     ec2.start_instances(InstanceIds=instances)
#     send_slack_notification('Styfiv2 stopped. Starting Styfiv2 Server.')
#     print 'started your instances: ' + str(instances)