import requests
import csv

arrayofdata = []
category_path = {}

csvfile = open('category_list.csv', 'w')
thedatawriter = csv.writer(csvfile)


def recurse(category_data, parent_name, parent_id, required_cat=None):
    for zxc in category_data["children"]:
        if not zxc["children"]:
            category = {"name": zxc["name"], "parent": parent_name, "parent_id": parent_id}
            category["parent"] += (" > " + zxc["name"])
            category["parent_id"] += (" > " + zxc["category_id"])
            category_path[zxc["category_id"]] = category["parent_id"]
            thedatawriter.writerow([category["parent"], zxc["category_id"]])
            print(category_path[zxc["category_id"]])
        else:
            zxc_name = parent_name + " > " + zxc["name"]
            zxc_id = parent_id + " > " + zxc["category_id"]
            recurse(zxc, zxc_name, zxc_id, required_cat)

headers = {'authorization': 'bearer eeey1lR3f48YxYUr9R3m7yQAYb74pDIrJiI'}
api_url = "http://www.styfi.in/api/product/category/tree"
category_response = requests.get(api_url, headers=headers).json()
data1 = category_response["data"]

for x in data1:
    p_name = x['name']
    p_cat = x["category_id"]
    recurse(x, p_name, p_cat)

