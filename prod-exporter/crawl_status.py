import json
import requests
from datetime import datetime, timedelta
from pymongo import MongoClient
import settings
import subprocess

database = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION]
data_category = requests.get(settings.API_URL + '/category', headers=settings.AUTHORIZATION_HEADER)
data_dict = json.loads(data_category.text)
category_data = data_dict['data']

data_brand_names = requests.get(settings.API_URL + '/brand', headers=settings.AUTHORIZATION_HEADER)
dict_brand_names = json.loads(data_brand_names.text)
brand_names = dict_brand_names['data']

yesterday = datetime.now() - timedelta(days=1)

subject = "Crawling Status for %s" % datetime.now().strftime('%d-%m-%y')


def get_category_brand_name(id_value, id_key, required_key, search_from):
    for each_item in search_from:
        if str(id_value) == each_item[id_key]:
            return each_item[required_key]

pipeline = [
    {
        "$match": {
            "extract_time": {"$gte": yesterday},
            "price": {"$exists": True},
            "images": {"$exists": True}
        }
    },
    {
        "$group": {
            "_id": {"store": "$store", "category_id": "$category_id", "brand_id": "$brand_id"},
            "count": {"$sum": 1}
        }
    }
]

body = '''
        <h3>Stats for Crawl</h3>
        <table style="width:100%;text-align: left;border: 1px solid black;">
          <tr>
            <th style="border: 1px solid black;">Website</th>
            <th style="border: 1px solid black;">Category</th>
            <th style="border: 1px solid black;">Brand Name</th>
            <th style="border: 1px solid black;">Count</th>
          </tr>
          '''


aggregate_response = database[settings.PRODUCT_COLLECTION].aggregate(pipeline=pipeline)

total_count = 0
for status in aggregate_response:
    product_detail = status['_id']
    count = status['count']
    website = product_detail['store']
    category_id = product_detail['category_id']
    brand_id = product_detail['brand_id']
    total_count += count

    category_name = get_category_brand_name(category_id, 'category_id', 'name', category_data)
    if category_name:
        category_name = category_name.replace(u'\xa0', ' ')
    else:
        category_name = 'Category Deleted.'
    brand_name = get_category_brand_name(brand_id, 'brand_id', 'brand_name', brand_names)
    if not brand_name:
        brand_name = 'Brand Deleted.'

    body += '''<tr><td style="border: 1px solid black;">''' + str(website) + '''</td>
        <td style="border: 1px solid black;">''' + str(category_name) + '''</td>
        <td style="border: 1px solid black;">''' + str(brand_name) + '''</td>
        <td style="border: 1px solid black;">''' + str(count) + '''</td></tr>'''


body += '''<tr><td colspan="3" style="border: 1px solid black;">Total</td>
        <td style="border: 1px solid black;">''' + str(total_count) + '''</td></tr></table>'''

process = subprocess.Popen(['mutt', '-e', 'set copy=no', '-e', 'set content_type=text/html', '-s', subject,
                            settings.RECIPIENT],
                           stdin=subprocess.PIPE)
process.communicate(body)
