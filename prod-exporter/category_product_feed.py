from pymongo import MongoClient
import requests
from datetime import datetime, timedelta
import json

product_feed_db = MongoClient('mongodb://35.154.19.246:27017')["styfiv2"]['product_feed_data']
prediction_db = MongoClient('mongodb://35.154.19.246:27017')["image_processing"]["prediction"]
AUTHORIZATION_HEADER = {'authorization': 'bearer hi1s3K6FrjZdUAUllXU6MCKve1xukvs5'}

category_dict = {}
category_name_dict = {}

data_category = requests.get('https://www.styfi.in/admin_api/category', headers=AUTHORIZATION_HEADER).json()
category_system = data_category['data']
for category in category_system:
    category_name = category['name'].replace(u'\xa0', ' ')
    category_dict[category['category_id']] = category_name
    category_name_dict[category_name] = category['category_id']

date_start = datetime.now() - timedelta(days=30)

for data in product_feed_db.find({}):
    try:
        image_url = data['product_image'][0]
        right_category_id = data['category_ids'][0]
        correct_category = category_dict[right_category_id]
    except Exception as e:
        print("Error occured for product url: %s" % data['store_data'][0]['url'])
    try:
        if right_category_id:
            if int(right_category_id) < 100 or int(right_category_id) > 176:
                category_num = 1
            else:
                category_num = 101
        else:
            category_num = None
        print('Sending API request.')
        category_data = requests.post('http://54.174.42.54:81/visualsearch/getproductid/',
                                      data=json.dumps({'imgUrl': image_url,
                                                       'category_id': category_num}),
                                      timeout=40).json()
        category_name_value = category_dict[category_data[0]['catagoryId']]
        prediction_score = []
        for index, p_score in enumerate(category_data):
            cat_name = category_dict[p_score['catagoryId']]
            cat_percent = float(p_score['score']) * 100
            prediction_score.append({'category_name': cat_name, 'score': cat_percent, 'index': index,
                                     'category_id': p_score['catagoryId']})
        print('Obtained prediction result.')
        correct_prediction = False
        index = -1
        if right_category_id:
            right_category = category_dict[right_category_id]
            right_category_score = 0
        for data_predict in prediction_score:
            if data_predict['index'] == 0:
                predicted_category = data_predict['category_name']
                prediction_score = data_predict['score']
            if correct_category == data_predict['category_name']:
                right_category = correct_category
                right_category_score = data_predict['score']
                index = data_predict['index']
                right_category_id = data_predict['category_id']
        if index == 0:
            correct_prediction = True
        predicted_category_id = category_name_dict[predicted_category]

        if image_url and predicted_category:
            prediction_feedback = {
                'predicted_category': predicted_category,
                'image_url': image_url,
                'predicted_category_id': predicted_category_id,
                'prediction_score': prediction_score,
                'right_category': right_category,
                'right_category_score': right_category_score,
                'right_category_id': right_category_id,
                'time_added': datetime.now(),
                'correct_prediction': correct_prediction,
                'index': index
            }
        print('Inserting into DB.')
        inserted_data = prediction_db.update_one({'image_url': image_url}, {"$set": prediction_feedback},
                                                 upsert=True)
        if inserted_data.upserted_id:
            message = 'Saved successfully!'
    except requests.exceptions.Timeout:
        print('Request Timeout')
