import boto3
import botocore
import logging
import re


region = 'ap-south-1'
access_id_rabbitmq = 'AKIAIBS6V5DCI4EKGVBQ'
access_key_rabbitmq = 's42odIJ89+3kQ7dWMy6pTe2lRWSASiY5GBAoLrkD'
group_id_rabbitmq = 'sg-3762915e'

access_id_mongodb = 'AKIAIV35EQC7XVCSPEXA'
access_key_mongodb = 'aULd+Ch0wEPzEC2UNdW0XSdjaXHeTMRWanxw70rO'
group_id_mongodb = 'sg-9c31cef5'

aws_instances = [
    {'region': region,
     'access_id':  access_id_rabbitmq,
     'access_key': access_key_rabbitmq,
     'group_id': group_id_rabbitmq,
     'port': 5672,
     'service': 'Rabbitmq'
     },
    {'region': region,
     'access_id': access_id_mongodb,
     'access_key': access_key_mongodb,
     'group_id': group_id_mongodb,
     'port': 27017,
     'service': 'Mongodb'
     },
]


def revoke_ip():
    ec2 = boto3.client('ec2', region_name=region,
                       aws_access_key_id=access_id_rabbitmq,
                       aws_secret_access_key=access_key_rabbitmq)
    addresses_dict = ec2.describe_security_groups()
    for eip_dict in addresses_dict['SecurityGroups']:
        if eip_dict['GroupId'] in [group_id_rabbitmq, group_id_mongodb]:
            ip_permissions = eip_dict['IpPermissions']
            for port_permission in ip_permissions:
                ip_list_to_revoke = []
                if port_permission['FromPort'] in [27017, 5672]:
                    ip_list_to_revoke = [re.findall('^(?:136|138|148).*$', x.values()[0])
                                         for x in port_permission['IpRanges']]
                    ip_list_to_revoke = [x[0] for x in ip_list_to_revoke if x]
                    if ip_list_to_revoke:
                        if port_permission['FromPort'] == 27017:
                            group_id = group_id_mongodb
                        else:
                            group_id = group_id_rabbitmq
                        try:
                            for ip in ip_list_to_revoke:
                                print('Revoking IP %s for %s port: %s' % (ip, group_id, port_permission['FromPort']))
                                ec2.revoke_security_group_ingress(GroupId=group_id,
                                                                  IpProtocol="tcp",
                                                                  CidrIp=ip,
                                                                  FromPort=port_permission['FromPort'],
                                                                  ToPort=port_permission['FromPort'])
                        except botocore.exceptions.ClientError as e:
                            logging.error('Unexpected error for service %s: %s' % (aws_instances[0]['service'], e))
                            break

revoke_ip()
