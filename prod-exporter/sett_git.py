import pika
import logging
import time


def setup_custom_logger(name):
    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)
    return logger

STYFI_MONGODB = 'mongodb://styfiadmin:tGaKMCEZyxBK3Cad@styfimongodbclusterone-shard-00-00-1tkzd.mongodb.net:27017,styfimongodbclusterone-shard-00-01-1tkzd.mongodb.net:27017,styfimongodbclusterone-shard-00-02-1tkzd.mongodb.net:27017/<DATABASE>?ssl=true&replicaSet=styfimongodbclusterone-shard-0&authSource=admin'
#STYFI_MONGODB = 'mongodb://nosql.styfi.in:27017'
# STYFI_MONGODB = 'mongodb://192.168.1.200:27017'

STYFI_COLLECTION = 'styfi_curated'

CATEGORY_COLLECTION = 'universal_category_live_' + time.strftime("%Y-%m-%d")

PRODUCT_COLLECTION = 'universal_live_' + time.strftime("%Y-%m-%d")

API_COLLECTION = 'api_live_' + time.strftime("%Y-%m-%d")
API_ERR_COLLECTION = 'api_error_' + time.strftime("%Y-%m-%d")
QUEUE_ERR_COLLECTION = 'queue_error_' + time.strftime("%Y-%m-%d")
PRODUCT_COLLECTION_WITHOUT_TIMESTAMP = 'universal_live_'
API_COLLECTION_WITHOUT_TIMESTAMP = 'api_live_'
# STATICFILES_DIRS = (
#     os.path.join(BASE_DIR, "static"),
# )

# Email notification for crawl status
RECIPIENT = "vaulstein@stylabs.in"

credentials = pika.PlainCredentials('stylabs_new', 'KB#Rxxqe2')
RABBITMQ_HOST = {'host': '127.0.0.1', 'port': 5672, 'credentials': credentials,
                 'heartbeat_interval': 0, 'vhost': 'prod_styfi'}

# RABBITMQ_HOST = {'host': '192.168.1.200', 'port': 5672, 'credentials': credentials,
#                  'heartbeat_interval': 0, 'vhost': 'prod_styfi'}

# credentials = pika.PlainCredentials('stylabs_new', 'KB#Rxxqe2')
# RABBITMQ_HOST = {'host': 'autocat.styfi.in', 'port': 5672, 'credentials': credentials,
#                 'heartbeat_interval': 0, 'vhost': ''}

# credentials = ''
# RABBITMQ_HOST = {'host': '192.168.1.200', 'port': 5672}

BASE_URL = 'https://autocat.lightbuzz.in/'

PARTNER_URL = 'http://prod.styfi.in/partners/'
#PARTNER_URL = 'http://middle.styfi.in/'
#PARTNER_URL = 'http://middle.styfi.in/styfi-partners/public/'

API_URL_2 = 'http://127.0.0.1/styfiv2/admin_api'

# API_URL_2 = 'http://192.168.1.200/styfiv2/admin_api'

#API_URL = 'https://www.styfi.in/admin_api'
API_URL = 'http://styfiv2.sl:8888/admin_api'

API_BETA_URL = 'http://alpha.styfi.in/admin_api'

#API_URL = 'http://192.168.1.200/styfiv2/admin_api'

APP_URL = 'http://styfiv2.sl:8888/api'

#IMAGE_P_URL = 'http://13.126.31.168:81'

IMAGE_P_URL = 'http://127.0.0.1:8011'

# IMAGE_P_URL = 'http://192.168.1.200:8011'

AUTHORIZATION_HEADER = {'authorization': 'bearer hi1s3K6FrjZdUAUllXU6MCKve1xukvs5'}

APP_HEADER = {'authorization': 'bearer eeey1lR3f48YxYUr9R3m7yQAYb74pDIrJiI'}

CATEGORY_URL = 'http://127.0.0.1:8089/autocat/api'

# CATEGORY_URL = 'https://192.168.1.200:8089/autocat/api'

ELASTIC_SEARCH_API = 'http://search-styfiprodnew-4hnrtct6fd2mgz47e2tbvramta.ap-south-1.es.amazonaws.com:80'

# Spider Settings Crawler
SPIDER_API_KEY = '8851c65d07cc432abb0ed5cd6cea0129'

SPIDER_ID = '76815'

USER_AGENTS = ('Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
               'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
               'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko',
               'Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16',
               'Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko')


SLACK_URL = 'https://hooks.slack.com/services/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'

PROCESS_TO_CHECK = {
    'RabbitMq': {'name': 'rabbitmq-server', 'service_name': 'S80rabbitmq-ser', 'allowed_states': ['running', 'sleeping']},
    'Apache': {'name': 'httpd', 'service_name': 'httpd', 'allowed_states': ['running', 'sleeping']},
    'Redis': {'name': 'redis-server', 'service_name': 'redis-server', 'allowed_states': ['running', 'sleeping']},
    'Node': {'service_name': 'node', 'allowed_states': ['running', 'sleeping']},
    'Memcache': {'name': 'memcached', 'service_name': 'memcached', 'allowed_states': ['running', 'sleeping']},
    'MongoDB': {'name': 'memcached', 'service_name': 'mongod', 'allowed_states': ['running', 'sleeping']},
    'Postgres': {'name': 'postgresql', 'service_name': 'postmaster', 'allowed_states': ['running', 'sleeping']}
}

QUEUE = {'category': 'product', 'product_details': 'details'}

PID_PREFIX = 'prod'
