#!/usr/bin/env python
import os
import sys
import pika
import time
import json
import settings
from pymongo import MongoClient, IndexModel, DESCENDING
import requests
import datetime
import math
import string
import random
import re
import signal
import argparse

# Checking if process is already running, if it is not, then starting
pid = str(os.getpid())
pidfile = "/tmp/recrawl_extract.pid"

logger = settings.setup_custom_logger('root')

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--spawn', type=int, choices=range(1, 9), nargs='?', help='spawn a thread')
args = vars(parser.parse_args())
if 'spawn' in args:
    spawn = args['spawn']
    if spawn:
        # max 9 threads
        pidfile = "/tmp/recrawl_extract" + str(spawn) + ".pid"

if os.path.isfile(pidfile):
    logger.warn('%s already exists, exiting' % pidfile)
    sys.exit()

open(pidfile, 'w').write(pid)
# Signal handler to clean resources on shutdown


def handler(signum, frame):
    logger.info('Received kill signal. Shutting Down.')
    os.unlink(pidfile)
    sys.exit(1)

signal.signal(signal.SIGTERM, handler)

try:

    store_data = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION][settings.PRODUCT_COLLECTION]
    api_data = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION][settings.API_COLLECTION]
    api_error = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION][settings.API_ERR_COLLECTION]
    queue_error = MongoClient(settings.STYFI_MONGODB)[settings.STYFI_COLLECTION][settings.QUEUE_ERR_COLLECTION]
    index_store = IndexModel([("category_id", -1), ("brand_id", -1)])
    index2_store = IndexModel([("extract_time", -1)])
    index3_store = IndexModel([("url", -1)])
    store_data.create_indexes([index_store, index2_store, index3_store])
    index1 = IndexModel([("product_store.store_name", -1), ("product_store.url", -1),
                         ("status", -1)],
                        name="status_v")
    index2 = IndexModel([("extract_time", -1)],
                        name="time")
    api_data.create_indexes([index1, index2])
    data_category = requests.get(settings.API_URL + '/category/category_children', headers=settings.AUTHORIZATION_HEADER).json()
    category_data = data_category['data']
    category_dict = {}
    for cat in category_data:
        category_dict[cat['category_id']] = cat

    if settings.credentials:
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port'], credentials=settings.credentials))
    else:
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=settings.RABBITMQ_HOST['host'], port=settings.RABBITMQ_HOST['port']))
    channel = connection.channel()

    channel.queue_declare(queue='recrawl:items', durable=True, arguments={'x-max-priority': 10})
    channel.basic_qos(prefetch_count=1)
    logger.info(' [*] Waiting for messages. To exit press CTrL+C')


    def find_whole_words(w):
        word = w.replace(u'\xa0', ' ')
        return re.compile(r'\b({0})\b'.format(word), flags=re.IGNORECASE).search


    def child_nodes(category_name, product_name):
        parent_child_categories = []
        for each_category in category_data:
            if category_name in each_category['name']:
                category_array = each_category['name'].split(u'\xa0>\xa0')
                last_child = category_array[-1]
                if last_child in product_name:
                    parent_child_categories.append(each_category)
        return parent_child_categories


    def get_category_filter(category_id):
        category_name_filter = {}
        for each_category in category_data:
            if str(category_id) == each_category['category_id']:
                category_name_filter['filter'] = each_category['filters']
                category_name_filter['name'] = each_category['name']
                return category_name_filter


    def unique_id_generator(prefix='', more_entropy=False):
        m = time.time()
        unique_id = '%8x%05x' % (math.floor(m), (m - math.floor(m)) * 1000000)
        if more_entropy:
            valid_chars = list(set(string.hexdigits.lower()))
            entropy_string = ''
            for i in range(0, 10, 1):
                entropy_string += random.choice(valid_chars)
            unique_id = unique_id + entropy_string
        unique_id = prefix + unique_id
        return unique_id


    def callback(ch, method, properties, body):
        headers = {
            'User-Agent':
                'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
        }

        dummy_product_data = {"product_description":
            {"1":
                {
                    "name": "",
                    "description": "",
                    "meta_title": "Product Meta",
                    "meta_description": "",
                    "meta_keyword": "",
                    "tag": ""
                }
            },
            "model": "",
            "sku": "",
            "upc": "",
            "ean": "",
            "jan": "",
            "isbn": "",
            "mpn": "",
            "location": "",
            "price": "",
            "tax_class_id": "",
            "quantity": "",
            "minimum": "",
            "subtract": "",
            "stock_status_id": "",
            "shipping": "",
            "keyword": "",
            "date_available": "",
            "length": "",
            "width": "",
            "height": "",
            "length_class_id": "",
            "weight": "",
            "weight_class_id": "",
            "status": "",
            "sort_order": "",
            "manufacturer": "",
            "manufacturer_id": "",
            "category": "",
            "product_category": [],
            "filter": "",
            "product_filter": [],
            "product_store": [
                {"store_id": "",
                 "price": "",
                 "original_price": "",
                 "url": "",
                 "discount": ""
                 }
            ],
            "download": "",
            "related": "",
            "option": "",
            "image": [],
            "points": "",
            "product_reward": {"1": {"points": ""}},
            "product_layout": ["", "", ""]
        }

        message = ''
        logger.debug(' [x] Received %r' % body)
        try:
            product = json.loads(body)
            final_product_data = dummy_product_data.copy()
            raw_text_data = ''

            assert isinstance(product, dict)
            test = product.copy()
            categorization_product = product.copy()
            assert isinstance(test, dict)

            final_product = {}

            # Removing attributes not required for processing
            for key_delete in [
                "_id", "url", "price", "name", "store", "category_id", "brand_id", "images", "category_id", "discount",
                'extract_time', "breadcrumbs", "styfi_category", 'mapped', 'new_data', "og_url", "features", "sizes",
                "discount_percent", "variants"
            ]:
                test.pop(key_delete, None)

            product.pop('new_data', None)

            if 'store_category_id' not in product:
                product['store_category_id'] = None

            if 'url' in product:
                if product['store'] == 'yepme' or product['store'] == 'koovs' or product['store'] == 'flipkart':
                    pass
                elif product['store'] == 'amazon' and 'ref=as_li_tl' not in product['url']:
                    try:
                        if '/ref' in product['url']:
                            product['url'] = re.sub(r"/ref=\w+", '/ref=as_li_tl?ie=UTF8&tag=wwwstyfiin-21', product['url'])
                        else:
                            product['url'] = product['url'].split('?')[0] + '/ref=as_li_tl?ie=UTF8&tag=wwwstyfiin-21'
                    except:
                        product['url'] = product['url'].split('?')[0] + '/ref=as_li_tl?ie=UTF8&tag=wwwstyfiin-21'
                    if 'qid' in product and product['qid']:
                        product['url'] += '&' + product['qid']
                # else:
                #     if '?' in product['url']:
                #         product['url'] = product['url'].split('?')[0]
                logger.info(product['url'])
            if 'is_partner_store_product' in test and ('category_id' not in product or not product['category_id']):
                try:
                    category_data_test = requests.post(settings.IMAGE_P_URL + '/vs/getproductattribute/',
                                                       data=json.dumps({'imgUrl': product['images'][0],
                                                                        'category_id': product['store_category_id']
                                                                        }), timeout=40).json()
                    product['category_id'] = category_data_test['predicatedId'][0]['label']
                    if 'color' not in product or 'colour' not in product:
                        product['color'] = category_data_test['colors'][0]
                except Exception as e:
                    product['category_id'] = product['store_category_id']
                    logger.error('Could not fetch category')
                if 'store_product_id' in test:
                    sku = test.get('store_sku', test['store_product_id']) or test['store_product_id']
                    product['sku'] = sku
                    test['sku'] = sku
            if 'sku' not in test:
                previous_saved = store_data.find_one({'url': product['url']})
                if previous_saved:
                    if 'sku' in previous_saved:
                        product['sku'] = previous_saved['sku']
                    else:
                        product['sku'] = unique_id_generator()
                    if 'assign_to' in previous_saved:
                        final_product_data['stylist'] = previous_saved['assign_to']
                else:
                    product['sku'] = unique_id_generator()
            else:
                test.pop("sku")

            if 'sold_out' in test:
                test.pop('sold_out')
                final_product_data['quantity'] = 0
                final_product_data['status'] = 0
                logger.info('Status set to 0 for url: %s' % product['sku'])
            elif 'discontinue' in test:
                final_product_data['status'] = -1
                logger.info('Product Discontinued.')
            else:
                final_product_data['status'] = 1
            if 'status' in product and product['status'] == -1:
                final_product_data['status'] = -1

            if 'assign_to' in product:
                final_product_data['stylist'] = product['assign_to']

            if 'variants' in product:
                final_product_data['variants'] = product['variants']
            if 'store_sku' in product:
                final_product_data['store_sku'] = product['store_sku']
            if 'store_product_id' in product:
                final_product_data['store_product_id'] = product['store_product_id']
            if 'is_partner_store_product' in product:
                final_product_data['is_partner_store_product'] = product['is_partner_store_product']
            if 'store_brand' in product:
                final_product_data['store_brand'] = product['store_brand']
            if 'tags' in product:
                final_product_data['tags'] = product['tags']
                if isinstance(product['tags'], list):
                    raw_text_data += " \n ".join(product['tags'])
                elif isinstance(product['tags'], basestring):
                    raw_text_data += product['tags']
            if 'colour' in product:
                if isinstance(product['colour'], list):
                    test['color'] = product['colour']
                else:
                    test['color'] = [product['colour']]
            if 'store_colour' in product:
                final_product_data['store_colour'] = product['store_colour']
            if 'product_id' in product:
                final_product_data['product_id'] = product['product_id']

            if 'color' in product:
                if isinstance(product['color'], list):
                    test['color'] = product['color']
                else:
                    test['color'] = [product['color']]
            if 'Color' in product:
                if isinstance(product['Color'], list):
                    test['color'] = product['Color']
                else:
                    test['color'] = [product['Color']]

            if 'color' in test:
                final_product_data['store_colour'] = test['color']

            if 'original_price' in test:
                final_product["original_price"] = test['original_price']
                test.pop("original_price")
            if 'before_price' in test:
                final_product["original_price"] = test['before_price']
                test.pop("before_price")
            if 'original_price' not in final_product and 'price' in product:
                final_product['original_price'] = product['price']
            if 'price' in product:
                if product['price'] == 0:
                    final_product_data['status'] = -2
                final_product['price'] = product['price']
                if final_product['price'] > final_product['original_price']:
                    final_product['original_price'] = final_product['price']
            else:
                final_product_data['status'] = 0

            if 'extract_time' in categorization_product:
                categorization_product.pop('extract_time')

            # try:
            #     payload = {'product': categorization_product}
            #     categorization_data = requests.post(settings.CATEGORY_URL, data=json.dumps(payload))
            #     if categorization_data:
            #         response_category = json.loads(categorization_data.text)
            #         if 'match' in response_category and isinstance(response_category['match'], list):
            #             styfi_category = response_category['match'][0][0]
            #             styfi_category = styfi_category.split(' > ')
            #             product['styfi_category'] = styfi_category
            #             final_product_data["styfi_category"] = styfi_category
            #             product['styfi_found'] = True
            # except Exception as e:
            #     pass

            attribute_keys = []
            for attrib, value in test.iteritems():
                attribute_keys.append(attrib)

            for attrib, value in test.iteritems():
                if value and attrib.lower() not in final_product:
                    if isinstance(value, list):
                        final_product[attrib.lower()] = value
                    else:
                        final_product[attrib.lower()] = [value]

            if 'name' in product:
                final_product_data["product_description"]["1"]["name"] = product["name"]
                raw_text_data += (product["name"].lower() + " ")
            if 'description' in product and product['description']:
                final_product_data["product_description"]["1"]["description"] = product["description"]
                raw_text_data += product["description"].lower()
            else:
                if 'brand' in test:
                    test.pop('brand')
                if 'description' in test:
                    test.pop('description')
                final_product['description'] = ''
                for attrib, value in test.iteritems():
                    if isinstance(value, list):
                        try:
                            final_product['description'] += (str(attrib).capitalize() +
                                                             ' : ' + str(','.join(value)) +
                                                             ' \n')
                        except (TypeError, UnicodeEncodeError):
                            pass
                if 'description' in final_product:
                    final_product_data["product_description"]["1"]["description"] = final_product["description"]
                    raw_text_data += final_product["description"].lower()
            if 'meta_description' in product:
                final_product_data["product_description"]["1"]["meta_description"] = product["meta_description"]

            # if 'sold_out' in product and product['sold_out']:
            #     final_product_data["quantity"] = 0
            if 'sizes' in product:
                final_product_data['sizes'] = product['sizes']
            if 'sku' in product:
                final_product_data['sku'] = product['sku']
            if 'price' in final_product:
                if final_product['price']:
                    final_product_data['price'] = final_product['price']
                    final_product_data['product_store'][0]['price'] = final_product['price']
                else:
                    final_product_data['price'] = 0
                    final_product_data['product_store'][0]['price'] = 0
            else:
                if 'original_price' in final_product:
                    final_product_data['price'] = final_product['original_price']
                    final_product_data['product_store'][0]['price'] = final_product['original_price']
            if 'original_price' in final_product:
                if final_product['original_price']:
                    final_product_data['product_store'][0]['original_price'] = final_product['original_price']
                else:
                    final_product_data['product_store'][0]['original_price'] = 0
            if 'store_id' in product:
                final_product_data['product_store'][0]['store_id'] = product['store_id']
            if 'url' in product:
                final_product_data['product_store'][0]['url'] = product['url']
            if 'store' in product:
                final_product_data['product_store'][0]['store_name'] = product['store']
            if 'sku' in product:
                final_product_data['product_store'][0]['sku'] = product['sku']
            if 'discount' in product:
                final_product_data['product_store'][0]['discount'] = product['discount']
            else:
                if ('original_price' in final_product and 'price' in final_product and
                        isinstance(final_product['original_price'], (int, float)) and
                        final_product['original_price'] != final_product['price']):
                    try:
                        discount = int((final_product['original_price'] - final_product['price']) / float(
                            final_product['original_price']) * 100)
                        final_product_data['product_store'][0]['discount_percent'] = discount
                        final_product_data['product_store'][0]['discount'] = '( ' + str(discount) + '% OFF)'
                    except Exception as e:
                        logger.info('Discount missing.')
            if 'discount_percent' in product:
                final_product_data['product_store'][0]['discount_percent'] = product['discount_percent']
            inventory_quantity = 0
            if 'variants' in product:
                for each_variant in product['variants']:
                    if 'inventory_quantity' in each_variant:
                        if each_variant['inventory_quantity'] > 0:
                            try:
                                inventory_quantity += int(each_variant['inventory_quantity'])
                            except ValueError:
                                logger.info('Inventory %s' % each_variant['inventory_quantity'])
                        else:
                            each_variant['inventory_quantity'] = 0
                product['inventory_quantity'] = inventory_quantity
                if 'inventory_quantity' in product:
                    if product['inventory_quantity'] == 0:
                        final_product_data['status'] = 0
                    elif product['inventory_quantity'] > 0:
                        final_product_data['status'] = 1
                    final_product_data['quantity'] = product['inventory_quantity']
                else:
                    final_product_data['quantity'] = 1
            else:
                final_product_data['quantity'] = 1
            if 'extract_time' in product and isinstance(product['extract_time'], datetime.datetime):
                final_product_data['data_available'] = json.dumps(product['extract_time'].isoformat())
            if 'images' in product:
                final_product_data['image'] = product['images']

            if 'brand_id' in product:
                final_product_data['manufacturer_id'] = product['brand_id']
            if 'category_id' in product:
                final_product_data['product_category'] = [product['category_id']]

            # Get filter values
            if 'product_filter' in product and product['product_filter']:
                if isinstance(product['product_filter'], basestring):
                    product_filter = []
                else:
                    product_filter = product['product_filter']
            else:
                product_filter = []
            filter_value_id_dict = {}
            data_name_filter = get_category_filter(product['category_id'])
            if data_name_filter:
                filter_values = data_name_filter['filter']
                category_name = data_name_filter['name']

            # child_node_names = child_nodes(category_name, product["name"])
            # if len(child_node_names) == 2:
            #     final_product_data['product_category'] = [child_node_names[1]['category_id']]
            #     product['category_id']
            exclude_description = ['Color']

            # Creating value-id pair and storing in filter_value_id_dict
            #  fetching id for feature and storing in product_filter
            # E.g. if product['Color'] = 'black' -> 'black'= '2' -> product_filter = ['2']
            if data_name_filter and isinstance(filter_values, dict):
                for name, values in filter_values.iteritems():
                    for filter_values_data in values:
                        if name not in exclude_description:
                            if filter_values_data['name'] in filter_value_id_dict:
                                filter_value_id_dict[filter_values_data['name']].append(filter_values_data['filter_id'])
                            else:
                                filter_value_id_dict[filter_values_data['name']] = [filter_values_data['filter_id']]
                        if name.lower() in final_product:
                            if any(filter_values_data['name'].lower() in s.lower() for s in
                                   final_product[name.lower()] if isinstance(s, basestring)):
                                try:
                                    product_filter.append(filter_values_data['filter_id'])
                                except Exception as e:
                                    filter_values_data['name']
                                    product_filter = []
                                    logger.warn('filter value unicode')
                                    logger.warn(filter_values_data['filter_id'])

            # Extracting Values from description (Full text search)
            for keys, values in filter_value_id_dict.items():
                if find_whole_words(keys.lower())(raw_text_data) and values[0] not in product_filter:
                    product_filter.extend(values)
            product_filter = list(set(product_filter))
            logger.debug(final_product_data)
            if final_product_data['image']:
                if 'product_id' in final_product_data:
                    logger.info('Product Id present. Calling Update API.')
                    final_product_data['product_filter'] = product_filter
                    try:
                        data = requests.post(settings.API_URL + '/product/update_product',
                                             data=json.dumps(final_product_data),
                                             headers=settings.AUTHORIZATION_HEADER, timeout=10)
                    except requests.exceptions.ReadTimeout:
                        data = None
                        logger.warn('Error in receiving response.')
                    final_product_data['extract_time'] = datetime.datetime.utcnow()
                    logger.info('Sending API call. 1')
                    api_inserted = api_data.insert_one(final_product_data)
                    if api_inserted.inserted_id:
                        logger.info('API data saved to DB.')
                elif 'stylist' in final_product_data:
                    logger.info('Stylist Id present. Assigning directly.')
                    final_product_data['product_filter'] = product_filter
                    try:
                        data = requests.post(settings.API_URL + '/product/make_live', data=json.dumps(final_product_data),
                                             headers=settings.AUTHORIZATION_HEADER, timeout=10)
                    except requests.exceptions.ReadTimeout:
                        data = None
                        logger.warn('Error in receiving response.')
                    final_product_data['extract_time'] = datetime.datetime.utcnow()
                    logger.info('Sending API call. 1')
                    api_inserted = api_data.insert_one(final_product_data)
                    if api_inserted.inserted_id:
                        logger.info('API data saved to DB.')
                else:
                    final_product_data['product_filter'] = product_filter
                    try:
                        data = requests.post(settings.API_URL + '/product/add_product',
                                             data=json.dumps(final_product_data),
                                             headers=settings.AUTHORIZATION_HEADER, timeout=10)
                    except requests.exceptions.ReadTimeout:
                        data = None
                        logger.warn('Error in receiving response.')
                    final_product_data['extract_time'] = datetime.datetime.utcnow()
                    api_inserted = api_data.insert_one(final_product_data)
                    logger.info('Sending API call. 2')
                    if api_inserted.inserted_id:
                        logger.info('API data saved to DB.')
            else:
                final_product_data['status'] = -1
                if 'sku' in product:
                    try:
                        data = requests.post(settings.API_URL + '/product/add_product', data=json.dumps(final_product_data),
                                             headers=settings.AUTHORIZATION_HEADER, timeout=10)
                    except requests.exceptions.ReadTimeout:
                        data = None
                        logger.warn('Error in receiving response.')
                    final_product_data['extract_time'] = datetime.datetime.utcnow()
                    api_inserted = api_data.insert_one(final_product_data)
                    logger.info('Sending API call.')
                    if api_inserted.inserted_id:
                        logger.info('API data saved to DB. 3')
                    logger.warn('Product image missing for %s' % product['sku'])
                else:
                    logger.warn('SKU missing for %s' % product['sku'])
            try:
                if data:
                    json_response = json.loads(data.text)
                    logger.debug(json_response)
                    if json_response['code'] == 200:
                        product['added'] = True
                        inserted_data = store_data.insert_one(product)
                        if inserted_data.inserted_id:
                            logger.info('Product added into DB.')
                            # store_data.update_one({'url': product['sku']}, {"$set": {"added": True}})
                    else:
                        api_error.insert_one(product)
                        logger.warn(data.text)
                        message = 'Product not sent to API: Url - %s' % product['sku']
                else:
                    api_error.insert_one(product)
                    logger.info('Image missing.')
                    # flag_error = False
            except ValueError:
                api_error.insert_one(product)
                logger.warn(data.text)
                message = 'Product not sent to API: Url - %s' % product['sku']
                # flag_error = False

            # if flag_error:
            #     message = 'Product sent successfully'
        except Exception as e:
            logger.warn('Failed queue. ')
            queue_error.insert_one(product)

        logger.info(message)
        logger.info(' [x] Item feed API call')
        ch.basic_ack(delivery_tag=method.delivery_tag)


    channel.basic_consume(callback,
                          queue='details:items')

    channel.start_consuming()
finally:
    connection.close()
    logger.info(' [x] Closing Connection to RabbitMQ.')
    if os.path.isfile(pidfile):
        os.unlink(pidfile)
