from __future__ import print_function
import elasticsearch
import elasticsearch.helpers
import elasticsearch.client
import pymysql
import requests
import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument("-a", "--all", help="import these data: categories, brands, tags, products", action="store_true")
parser.add_argument("-b", "--brands", help="import all brands", action="store_true")
parser.add_argument("-c", "--categories", help="import all categories", action="store_true")
parser.add_argument("-p", "--products", help="import all products", action="store_true")
parser.add_argument("-f", "--products_full", help="import all products full", action="store_true")
parser.add_argument("-t", "--tags", help="import all tags", action="store_true")
parser.add_argument("-u", "--user_scores", help="import all user_scores", action="store", default=None)
args = parser.parse_args()

sql_client = pymysql.connect(host='styfi-rds.cbjdlzx4uat7.ap-south-1.rds.amazonaws.com', user="styfidbadmin", password="FQy5wlUSiC",port=3306, database="styfiv2",
                             cursorclass=pymysql.cursors.DictCursor)

# elastic = elasticsearch.Elasticsearch("192.168.1.200")
#elastic = elasticsearch.Elasticsearch("elastic.styfi.in")
#elastic = elasticsearch.Elasticsearch("https://elastic:HYCN0t2LxsXEpu9R6oaZm7Xa@c80aa9d75c9ca328bb165424f1c56416.ap-southeast-1.aws.found.io:9243")
elastic = elasticsearch.Elasticsearch("https://search-styfi-a43wachlj7ilooaogmsnriynqi.ap-south-1.es.amazonaws.com:443")

indices = elasticsearch.client.IndicesClient(elastic)
headers = {"authorization": "bearer eeey1lR3f48YxYUr9R3m7yQAYb74pDIrJiI"}

if args.all:
    print("all arguments enabled")
    args.brands = True
    args.categories = True
    args.products = True
    args.tags = True

if args.brands:
    print("brands enabled")
    if not indices.exists("sl_brands"):
        indices.create('sl_brands', body={
            "settings": {
                "analysis": {
                    "analyzer": {
                        "autocomplete": {
                            "tokenizer": "autocomplete",
                            "filter": [
                                "lowercase"
                            ]
                        },
                        "autocomplete_search": {
                            "tokenizer": "lowercase"
                        }
                    },
                    "tokenizer": {
                        "autocomplete": {
                            "type": "edge_ngram",
                            "min_gram": 2,
                            "max_gram": 10,
                            "token_chars": [
                                "letter"
                            ]
                        }
                    }
                }
            },
            "mappings": {
                "brand": {
                    "properties": {
                        "name": {
                            "type": "string",
                            "analyzer": "autocomplete",
                            "search_analyzer": "autocomplete_search"
                        }
                    }
                }
            }
        })

    with sql_client.cursor() as cursor:
        sql = """SELECT * FROM oc_manufacturer"""
        cursor.execute(sql)
        results = cursor.fetchall()
        for result in results:
            print(result)
            elastic.index("sl_brands", "brand", {"name": result["name"], "gender": result["gender"]},
                          id=result["manufacturer_id"])

if args.categories:
    print("categories enabled")
    if not indices.exists("sl_categories"):
        indices.create("sl_categories", body={
            "settings": {
                "analysis": {
                    "analyzer": {
                        "autocomplete": {
                            "tokenizer": "autocomplete",
                            "filter": [
                                "lowercase"
                            ]
                        },
                        "autocomplete_search": {
                            "tokenizer": "lowercase"
                        }
                    },
                    "tokenizer": {
                        "autocomplete": {
                            "type": "edge_ngram",
                            "min_gram": 2,
                            "max_gram": 10,
                            "token_chars": [
                                "letter"
                            ]
                        }
                    }
                }
            },
            "mappings": {
                "category": {
                    "properties": {
                        "name": {
                            "type": "string",
                            "analyzer": "autocomplete",
                            "search_analyzer": "autocomplete_search"
                        },
                        "parent": {
                            "type": "string",
                            "index": "no"
                        }
                    }
                }
            }
        })

    def recurse(category_data, parent_name):
        for zxc in category_data["children"]:
            if not zxc["children"]:
                category = {"name": zxc["name"], "parent": parent_name}
                print(category)
                elastic.index("sl_categories", "category", category, id=zxc["category_id"])
            else:
                zxc_name = parent_name + " > " + zxc["name"]
                recurse(zxc, zxc_name)


    api_url = "https://alpha.styfi.in/api/product/category/tree"
    category_response = requests.get(api_url, headers=headers).json()
    data1 = category_response["data"]

    for x in data1:
        name = x["name"]
        recurse(x, name)

if args.tags:
    print("tags enabled")
    with sql_client.cursor() as cursor:
        for tag_group in [1, 3, 5]:
            sql = """SELECT DISTINCT ofg.`name` AS filter_group, of.filter_id AS filter_id, ofd.`name` AS filter_name
                FROM
                `oc_filter` AS of,
                `oc_filter_description` AS ofd,
                `oc_filter_group_description` AS ofg
                WHERE
                of.filter_id = ofd.`filter_id` AND
                of.`filter_group_id` = ofg.`filter_group_id` AND
                ofg.`filter_group_id` IN (1,3,5)
            """
            cursor.execute(sql)
            for result in cursor.fetchall():
                elastic.index("sl_tags", "tag", result, id=result["filter_id"])


if args.products:
    print("products enabled")
    api_url = "https://alpha.styfi.in/admin_api/product/get_elastic_search_products"
    headers = {"authorization": "bearer hi1s3K6FrjZdUAUllXU6MCKve1xukvs5"}
    params = {"page": 1, "limit": 100}
    response = requests.get(api_url, headers=headers, params=params).json()

    if not indices.exists("sl_products"):
        indices.create("sl_products", body={
            "settings": {
                "analysis": {
                    "analyzer": {
                        "autocomplete": {
                            "tokenizer": "autocomplete",
                            "filter": [
                                "lowercase"
                            ]
                        },
                        "autocomplete_search": {
                            "tokenizer": "lowercase"
                        }
                    },
                    "tokenizer": {
                        "autocomplete": {
                            "type": "edge_ngram",
                            "min_gram": 2,
                            "max_gram": 10,
                            "token_chars": [
                                "letter"
                            ]
                        }
                    }
                }
            },
            "mappings": {
                "product": {
                    "properties": {
                        "categories": {
                            "properties": {
                                "category_id": {
                                    "type": "short"
                                },
                                "level": {
                                    "type": "short"
                                },
                                "name": {
                                    "type": "string"
                                }
                            }
                        },
                        "date_added": {
                            "type": "date",
                            "format": "yyy-MM-dd HH:mm:ss"
                        },
                        "date_modified": {
                            "type": "date",
                            "format": "yyy-MM-dd HH:mm:ss"
                        },
                        "filters": {
                            "properties": {
                                "filter_id": {
                                    "type": "integer"
                                },
                                "name": {
                                    "type": "string"
                                }
                            }
                        },
                        "collections": {
                            "properties": {
                                "styfi_collection_id": {
                                    "type": "integer"
                                },
                                "sort_order": {
                                    "type": "integer"
                                }
                            }
                        },
                        "inspires": {
                            "properties": {
                                "inspire_id": {
                                    "type": "integer"
                                },
                                "sort_order": {
                                    "type": "integer"
                                }
                            }
                        },
                        "manufacturer_id": {
                            "type": "integer"
                        },
                        "manufacturer_name": {
                            "type": "string",
                        },
                        "name": {
                            "type": "string",
                            "analyzer": "autocomplete",
                            "search_analyzer": "autocomplete_search"
                        },
                        "description": {
                            "type": "string",
                        },
                        "assigned_to": {
                            "type": "integer"
                        },
                        "tagged_by": {
                            "type": "integer"
                        },
                        "tagged": {
                            "type": "integer"
                        },
                        "is_offline": {
                            "type": "integer"
                        },
                        "product_id": {
                            "type": "string"
                        },
                        "size_available": {
                            "type": "string"
                        },
                        "quantity": {
                            "type": "integer"
                        },
                        "status": {
                            "type": "byte"
                        },
                        "stores": {
                            "properties": {
                                "original_price": {
                                    "type": "float"
                                },
                                "price": {
                                    "type": "float"
                                },
                                "sku": {
                                    "type": "string"
                                },
                                "store_id": {
                                    "type": "short"
                                },
                                "store_name": {
                                    "type": "string"
                                },
                                "url": {
                                    "type": "string",
                                    "index": "no"
                                }
                            }
                        },
                        "tag": {
                            "type": "string"
                        }
                    }
                }
            }
        })


def send_products():
    global response
    while response["data"]:
        print(response["data"])
        print("Page number: %s" % params["page"])
        for product in response["data"]:
            print(product)
            print(product["product_id"])
            if product["date_modified"] == "0000-00-00 00:00:00":
                del product["date_modified"]

            yield {'_op_type': "index", '_index': 'sl_products', '_type': 'product', '_id': product["product_id"],
                   '_source': product}
            # elastic.index("sl_products", "product", product, product["product_id"])
        params["page"] += 1
        response = requests.get(api_url, headers=headers, params=params).json()


success, failed = 0, 0
errors = []
for x, y in elasticsearch.helpers.streaming_bulk(elastic, send_products()):
    if not x:
        print(y)
        errors.append(y)
        failed += 1
    else:
        success += 1

print(success, failed)

if args.user_scores:
    print("user scores enabled")
    print(args.user_scores)
    if not indices.exists('sl_user_scores'):
        indices.create("sl_user_scores", body={
            "mappings": {
                "score": {
                    "properties": {
                        "category": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "firstname": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "lastname": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "brand": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "tag": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "gender": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "manufacturer": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "manufacturer_id": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "category_id": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "score": {
                            "type": "float",
                            "index": "not_analyzed"
                        }
                    }
                }
            }
        })

    with sql_client.cursor() as cursor:
        sql = """SELECT
  scs.score_id AS id,
  scs.customer_id,
  oc.gender,
  scs.manufacturer_id,
  scs.category_id,
  scs.filter_id AS tag_id,
  ocf.filter_group_id AS tag_group,
  scs.score,
  scs.manufacturer_score,
  scs.category_score,
  (SELECT group_concat(path_id) FROM oc_category_path WHERE category_id = scs.category_id GROUP BY category_id) AS categories
FROM
  sl_customer_score AS scs,
  oc_customer AS oc,
  oc_filter AS ocf
WHERE
  scs.customer_id = oc.customer_id AND
  scs.filter_id = ocf.filter_id AND
  scs.score_id > 2749357
"""
        if int(args.user_scores) > 0:
            sql += " AND scs.customer_id=%s" % args.user_scores

        cursor.execute(sql)


        def send_data():
            for my_object in cursor.fetchall():
                print(my_object['id'])
                if my_object['categories']:
                    my_object["categories"] = my_object["categories"].split(",")
                else:
                    my_object['categories'] = 0
                try:
                    data = {'_op_type': "index", '_index': 'sl_user_scores', '_type': 'score', '_id': my_object["id"],
                            '_source': json.dumps(my_object, encoding='utf-8')}
                except Exception as e:
                    data = {'_op_type': "index", '_index': 'sl_user_scores', '_type': 'score', '_id': my_object["id"],
                            '_source': json.dumps(my_object, encoding='latin-1')}
                    print(data)
                    print(e)
                    # raise Exception
                yield data


        success, failed = 0, 0
        errors = []

for ok, item in elasticsearch.helpers.streaming_bulk(elastic, send_data()):
    if not ok:
        errors.append(item)
        failed += 1
    else:
        success += 1

print(success, failed)
