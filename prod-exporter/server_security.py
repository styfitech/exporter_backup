import subprocess
from datetime import datetime


def send_mail(subject, recipient, body):
    process = subprocess.Popen(['mutt', '-e', 'set copy=no', '-e', 'set content_type=text/html', '-s',
                                subject, recipient],
                               stdin=subprocess.PIPE)
    process.communicate(body)

date_search = datetime.now().strftime('%m/%d/%Y')

message_all = subprocess.check_output(['/sbin/aureport', '--input-logs', '-ts', date_search], shell=False)
message_all = message_all.replace('\n', '<br>')
send_mail('Prod Server report', 'vaulstein@stylabs.in,anurag@stylabs.in,mohan@stylabs.in', message_all)

message_login = subprocess.check_output(['/sbin/aureport', '--input-logs', '-au', '-i', '--success', '-ts',
                                         date_search], shell=False)
message_login = message_login.replace('\n', '<br>')
send_mail('Prod Server login report', 'vaulstein@stylabs.in,anurag@stylabs.in,mohan@stylabs.in', message_login)

