import requests
from lxml import html
import json
import csv

fname = '../../Desktop/lbug.txt'

writer = csv.writer(open('../../Desktop/lbug.csv', 'wb'))
header = ['URL', 'PRODUCT ID', 'VARIANT ID']

with open(fname) as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]

for url in content:
    response = requests.get(url)
    tree = html.fromstring(response.content)
    variants_json = tree.xpath('//form[@class="variations_form cart"]/@data-product_variations')[0]
    variant_values = json.loads(variants_json)
    variant_id = variant_values[0]['variation_id']
    product_id = tree.xpath('//input[@name="product_id"]/@value')[0]
    writer.writerow([url, product_id, variant_id])

